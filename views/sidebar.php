<?php
if ( !defined('IN_CMS') ) {
    exit();
}
?>
<p class="button">
    <a href="<?php echo get_url('plugin/social_login/identities'); ?>">
        <img src="<?php echo PLUGINS_URI; ?>social_login/icons/32/tag.png" align="middle" alt="photo icon" /> <?php echo __('Social identities'); ?>
    </a>
</p>
<p class="button">
    <a href="<?php echo get_url('plugin/social_login/pending'); ?>">
        <img src="<?php echo PLUGINS_URI; ?>social_login/icons/32/tag.png" align="middle" alt="photo icon" /> <?php echo __('Pending email registrations'); ?>
    </a>
</p>
<p class="button">
    <a href="<?php echo get_url('plugin/social_login/documentation'); ?>">
        <img src="<?php echo PLUGINS_URI; ?>social_login/icons/32/document-help.png" align="middle" alt="<?php echo __('Documentation'); ?>" /> <?php echo __('Documentation'); ?>
    </a>
</p>
<?php if ( AuthUser::hasRole('administrator') ): ?>
    <p class="button">
        <a href="<?php echo get_url('plugin/social_login/settings'); ?>">
            <img src="<?php echo PLUGINS_URI; ?>social_login/icons/32/settings.png" align="middle" alt="settings icon" /> <?php echo __('Settings'); ?>
        </a>
    </p>
<?php endif; ?>


<?php
/*
 * SIDEBAR BOX
 * =========================================================================
 */
$mm_sbox = new View(mmCore::VIEW_FOLDER . 'mm_sbox', array(
            // optional parameters
            ));
echo $mm_sbox;
/*
 * =========================================================================
 * END OF SIDEBAR BOX
 */
?>


<?php
Observer::notify('mmtags_sidebar_javascript', array(
            'on_success' => "$('#album_id').trigger('change');"
));
?>