<?php
if ( !defined('IN_CMS') ) {
    exit();
}
Page::getQueryCount();
?>
<?php Observer::notify('mm_core_stylesheet'); ?>
<style>
    samp {background-color: greenyellow; padding: 0 4px; }
</style>
<h1><?php echo __('Settings'); ?></h1>
<?php if ( AuthUser::hasRole('administrator') ): ?>
    <div id="mm_plugin">
        <form action="<?php echo get_url('plugin/social_login/settings_store'); ?>" method="POST">
            <?php
            /**
             * GENERAL SETTINGS
             */
            ?>
            <fieldset >
                <legend><?php echo __('General settings'); ?></legend>
                <table class="settings">
                    <tr>
                        <td class="label wide">
                            <label for="setting_debug_mode">
                                <?php echo __('Debug mode'); ?>
                            </label>
                        </td>
                        <td class="field">
                            <select class="full" id="setting_debug_mode" name="settings[debug_mode]">
                                <option value="1" <?php echo (isset($settings['debug_mode']) && ($settings['debug_mode'] == '1')) ? 'selected="selected"' : ''; ?>><?php echo __('On'); ?></option>
                                <option value="0" <?php echo (isset($settings['debug_mode']) && ($settings['debug_mode'] == '0')) ? 'selected="selected"' : ''; ?>><?php echo __('Off'); ?></option>
                            </select>
                        </td>
                        <td class="comment one-third">
                            <?php echo __('Turn on to see debug messages.'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label wide">
                            <label for="setting_newuser_roles">
                                New User Roles
                            </label>
                        </td>
                        <td class="field">
                            <input class="full" id="setting_newuser_roles" name="settings[newuser_roles]" type="text" value="<?php echo isset($settings['newuser_roles']) ? $settings['newuser_roles'] : ''; ?>" />
                            <div>
                                <?php echo __('Currently available roles'); ?>
                            </div>
                            <div>
                                <?php
                                $roles = Record::findAllFrom('Role');
                                echo implode(', ', $roles);
                                ?>
                            </div>
                        </td>
                        <td class="comment one-third">
                            Assign these roles for newly registered Users.
                            Defaults to <samp>'social_user'</samp>
                            Separate with commas.
                            You should be careful in assigning other roles.
                        </td>
                    </tr>
                    <tr>
                        <td class="label wide">
                            <label>
                                AuthUser constants
                            </label>
                        </td>
                        <td class="field">
                            <i>ALLOW_LOGIN_WITH_EMAIL        </i> = <b><?php var_export(AuthUser::ALLOW_LOGIN_WITH_EMAIL) ?></b><br/>
                            <i>DELAY_ON_INVALID_LOGIN        </i> = <b><?php var_export(AuthUser::DELAY_ON_INVALID_LOGIN) ?></b><br/>
                            <i>DELAY_ONCE_EVERY              </i> = <b><?php var_export(AuthUser::DELAY_ONCE_EVERY) ?></b><br/>
                            <i>DELAY_FIRST_AFTER             </i> = <b><?php var_export(AuthUser::DELAY_FIRST_AFTER) ?></b><br/>
                        </td>
                        <td class="comment one-third">
                            You can adjust those values by editing Wolf Core file: <br/>
                            <samp>/wolf/app/models/AuthUser.php</samp>
                        </td>
                    </tr>
                </table>
            </fieldset>

            <div class="form-area">
                <div class="content tabs">
                    <ul class="tabNavigation">
                        <li class="tab"><a href="#email-settings"><?php echo __('E-mail'); ?></a></li>
                        <li class="tab"><a href="#google-settings"><?php echo __('Google'); ?></a></li>
                        <li class="tab"><a href="#facebook-settings"><?php echo __('Facebook'); ?></a></li>
                        <li class="tab"><a href="#twitter-settings"><?php echo __('Twitter'); ?></a></li>
                        <li class="tab"><a href="#github-settings"><?php echo __('Github'); ?></a></li>
                    </ul>

                    <div class="pages">
                        <div id="email-settings" class="page">
                            <?php
                            /**
                             * EMAIL
                             */
                            ?>

                            <table class="settings full">
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_email_enabled">
                                            Allow email registration?
                                        </label>
                                    </td>
                                    <td class="field">
                                        <select class="full" id="setting_email_enabled" name="settings[email_enabled]">
                                            <option value="1" <?php echo (isset($settings['email_enabled']) && ($settings['email_enabled'] == '1')) ? 'selected="selected"' : ''; ?>><?php echo __('Yes'); ?></option>
                                            <option value="0" <?php echo (isset($settings['email_enabled']) && ($settings['email_enabled'] == '0')) ? 'selected="selected"' : ''; ?>><?php echo __('No'); ?></option>
                                        </select>
                                    </td>
                                    <td class="comment one-third">
                                        Allow registering via email?
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_min_password_length">
                                            Min. password length
                                        </label>
                                    </td>
                                    <td class="field">
                                        <input class="full" id="setting_min_password_length" name="settings[min_password_length]" type="number" value="<?php echo isset($settings['min_password_length']) ? $settings['min_password_length'] : ''; ?>" />
                                    </td>
                                    <td class="comment one-third">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_min_username_length">
                                            Min. username and name length
                                        </label>
                                    </td>
                                    <td class="field">
                                        <input class="full" id="setting_min_username_length" name="settings[min_username_length]" type="number" value="<?php echo isset($settings['min_username_length']) ? $settings['min_username_length'] : ''; ?>" />
                                    </td>
                                    <td class="comment one-third">
                                    </td>
                                </tr>
                            </table>
                            <p>
                                <b>E-mail</b> authorization is a classical approach of user registration where the guest provides name, username, email and password.
                                Then the user receives <b>activation email</b>
                            </p>

                        </div> <!-- page -->
                        <div id="google-settings" class="page">
                            <?php
                            /**
                             * GOOGLE
                             */
                            ?>
                            <p>
                                To use Google Authorization you need to create Application for your website. Go to:
                                <a href="https://code.google.com/apis/console/b/0/">https://code.google.com/apis/console/b/0/</a>
                            </p>
                            <p>
                                There you need to set up your application (project) info.
                                In <b>API Access</b> tab Create <b>OAuth 2.0 Client ID</b> for Web Application
                                and set:<br/>
                                <b>Redirect URI</b> to: <samp><?php echo URL_PUBLIC . SOCIAL_FRONTEND_URI; ?>/google</samp><br/>
                            </p>
                            <p>
                                Copy <samp>Client ID</samp> and <samp>Client Secret</samp> to according fields below.
                            </p>
                            <p>
                                <a href="http://marekmurawski.pl/static/wolfplugins/social_login/google_setup.png" target="_blank">See screenshot here</a>
                            </p>
                            <table class="settings full">
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_google_enabled">
                                            Allow google OAuth?
                                        </label>
                                    </td>
                                    <td class="field">
                                        <select class="full" id="setting_google_enabled" name="settings[google_enabled]">
                                            <option value="1" <?php echo (isset($settings['google_enabled']) && ($settings['google_enabled'] == '1')) ? 'selected="selected"' : ''; ?>><?php echo __('Yes'); ?></option>
                                            <option value="0" <?php echo (isset($settings['google_enabled']) && ($settings['google_enabled'] == '0')) ? 'selected="selected"' : ''; ?>><?php echo __('No'); ?></option>
                                        </select>
                                    </td>
                                    <td class="comment one-third">
                                        Enabled?
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_google_client_id">
                                            Client ID
                                        </label>
                                    </td>
                                    <td class="field">
                                        <input class="full" id="setting_google_client_id" name="settings[google_client_id]" type="text" value="<?php echo isset($settings['google_client_id']) ? $settings['google_client_id'] : ''; ?>" />
                                    </td>
                                    <td class="comment one-third">
                                        Client ID
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_google_client_secret">
                                            Client Secret
                                        </label>
                                    </td>
                                    <td class="field">
                                        <input class="full" id="setting_google_client_secret" name="settings[google_client_secret]" type="text" value="<?php echo isset($settings['google_client_secret']) ? $settings['google_client_secret'] : ''; ?>" />
                                    </td>
                                    <td class="comment one-third">
                                        Client Secret
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_google_scope">
                                            Scope
                                        </label>
                                    </td>
                                    <td class="field">
                                        <input class="full" id="setting_google_scope" name="settings[google_scope]" type="text" value="<?php echo isset($settings['google_scope']) ? $settings['google_scope'] : ''; ?>" />
                                    </td>
                                    <td class="comment one-third">
                                        Separate with spaces
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_google_offline">
                                            Offline access
                                        </label>
                                    </td>
                                    <td class="field">
                                        <select class="full" id="setting_google_offline" name="settings[google_offline]">
                                            <option value="1" <?php echo (isset($settings['google_offline']) && ($settings['google_offline'] == '1')) ? 'selected="selected"' : ''; ?>><?php echo __('Yes'); ?></option>
                                            <option value="0" <?php echo (isset($settings['google_offline']) && ($settings['google_offline'] == '0')) ? 'selected="selected"' : ''; ?>><?php echo __('No'); ?></option>
                                        </select>
                                    </td>
                                    <td class="comment one-third">
                                        For future use, set to No for now.
                                    </td>
                                </tr>
                            </table>
                            <p>
                                <b>Scope</b> is a list of information your website will ask user to provide. For Google default is:
                            </p>
                            <p>
                                <textarea>https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile</textarea>
                            </p>

                        </div> <!-- page -->
                        <div id="facebook-settings" class="page">
                            <?php
                            /**
                             * FACEBOOK
                             */
                            ?>
                            <p>
                                To use Facebook Authorization you need to create Application for your website. Go to:
                                <a href="https://developers.facebook.com/apps">https://developers.facebook.com/apps</a>
                            </p>
                            <p>
                                Then setup your application. In Application Basic settings you need to provide
                                your <b>App Domain</b> and set it to your website's domain.
                                You also need to copy <b>App ID</b> and <b>App Secret</b> to according fields below
                            </p>
                            <p>
                                <a href="http://marekmurawski.pl/static/wolfplugins/social_login/facebook_setup.png" target="_blank">See screenshot here</a>
                            </p>
                            <table class="settings full">
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_facebook_enabled">
                                            Use facebook OAuth?
                                        </label>
                                    </td>
                                    <td class="field">
                                        <select class="full" id="setting_facebook_enabled" name="settings[facebook_enabled]">
                                            <option value="1" <?php echo (isset($settings['facebook_enabled']) && ($settings['facebook_enabled'] == '1')) ? 'selected="selected"' : ''; ?>><?php echo __('Yes'); ?></option>
                                            <option value="0" <?php echo (isset($settings['facebook_enabled']) && ($settings['facebook_enabled'] == '0')) ? 'selected="selected"' : ''; ?>><?php echo __('No'); ?></option>
                                        </select>
                                    </td>
                                    <td class="comment one-third">
                                        Enabled?
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_facebook_client_id">
                                            App ID
                                        </label>
                                    </td>
                                    <td class="field">
                                        <input class="full" id="setting_facebook_client_id" name="settings[facebook_client_id]" type="text" value="<?php echo isset($settings['facebook_client_id']) ? $settings['facebook_client_id'] : ''; ?>" />
                                    </td>
                                    <td class="comment one-third">
                                        App ID
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_facebook_client_secret">
                                            App Secret
                                        </label>
                                    </td>
                                    <td class="field">
                                        <input class="full" id="setting_facebook_client_secret" name="settings[facebook_client_secret]" type="text" value="<?php echo isset($settings['facebook_client_secret']) ? $settings['facebook_client_secret'] : ''; ?>" />
                                    </td>
                                    <td class="comment one-third">
                                        App Secret
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_facebook_scope">
                                            Scope
                                        </label>
                                    </td>
                                    <td class="field">
                                        <input class="full" id="setting_facebook_scope" name="settings[facebook_scope]" type="text" value="<?php echo isset($settings['facebook_scope']) ? $settings['facebook_scope'] : ''; ?>" />
                                    </td>
                                    <td class="comment one-third">
                                        Separate with spaces
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_facebook_offline">
                                            Offline access
                                        </label>
                                    </td>
                                    <td class="field">
                                        <select class="full" id="setting_facebook_offline" name="settings[facebook_offline]">
                                            <option value="1" <?php echo (isset($settings['facebook_offline']) && ($settings['facebook_offline'] == '1')) ? 'selected="selected"' : ''; ?>><?php echo __('Yes'); ?></option>
                                            <option value="0" <?php echo (isset($settings['facebook_offline']) && ($settings['facebook_offline'] == '0')) ? 'selected="selected"' : ''; ?>><?php echo __('No'); ?></option>
                                        </select>
                                    </td>
                                    <td class="comment one-third">
                                        For future use, set to No for now.
                                    </td>
                                </tr>

                            </table>
                            <p>
                                <b>Scope</b> is a list of information your website will ask user to provide. For Facebook default is:
                                <textarea>email</textarea>
                            </p>

                        </div> <!-- page -->
                        <div id="twitter-settings" class="page">
                            <?php
                            /**
                             * TWITTER
                             */
                            ?>
                            <p>
                                To use Twitter Authorization you need to create Application for your website. Go to:
                                <a href="https://dev.twitter.com/apps">https://dev.twitter.com/apps</a>
                            </p>
                            <p>
                                There you need to set up your application (project) info.
                                After completing setup. Make sure you set <b>Callback URL</b> in <b>Settings</b> tab to:
                                <samp><?php echo URL_PUBLIC . SOCIAL_FRONTEND_URI; ?>/twitter</samp>
                            </p>
                            <p>
                                Copy your application <b>Consumer key</b> and <b>Consumer secret</b> to according fields below
                            </p>
                            <p>
                                <a href="http://marekmurawski.pl/static/wolfplugins/social_login/twitter_setup.png" target="_blank">See screenshot here</a>
                            </p>
                            <table class="settings full">
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_twitter_enabled">
                                            Use twitter OAuth?
                                        </label>
                                    </td>
                                    <td class="field">
                                        <select class="full" id="setting_twitter_enabled" name="settings[twitter_enabled]">
                                            <option value="1" <?php echo (isset($settings['twitter_enabled']) && ($settings['twitter_enabled'] == '1')) ? 'selected="selected"' : ''; ?>><?php echo __('Yes'); ?></option>
                                            <option value="0" <?php echo (isset($settings['twitter_enabled']) && ($settings['twitter_enabled'] == '0')) ? 'selected="selected"' : ''; ?>><?php echo __('No'); ?></option>
                                        </select>
                                    </td>
                                    <td class="comment one-third">
                                        Enabled?
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_twitter_client_id">
                                            Consumer key
                                        </label>
                                    </td>
                                    <td class="field">
                                        <input class="full" id="setting_twitter_client_id" name="settings[twitter_client_id]" type="text" value="<?php echo isset($settings['twitter_client_id']) ? $settings['twitter_client_id'] : ''; ?>" />
                                    </td>
                                    <td class="comment one-third">
                                        Consumer key
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_twitter_client_secret">
                                            Consumer secret
                                        </label>
                                    </td>
                                    <td class="field">
                                        <input class="full" id="setting_twitter_client_secret" name="settings[twitter_client_secret]" type="text" value="<?php echo isset($settings['twitter_client_secret']) ? $settings['twitter_client_secret'] : ''; ?>" />
                                    </td>
                                    <td class="comment one-third">
                                        Consumer secret
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_twitter_offline">
                                            Offline access
                                        </label>
                                    </td>
                                    <td class="field">
                                        <select class="full" id="setting_twitter_offline" name="settings[twitter_offline]">
                                            <option value="1" <?php echo (isset($settings['twitter_offline']) && ($settings['twitter_offline'] == '1')) ? 'selected="selected"' : ''; ?>><?php echo __('Yes'); ?></option>
                                            <option value="0" <?php echo (isset($settings['twitter_offline']) && ($settings['twitter_offline'] == '0')) ? 'selected="selected"' : ''; ?>><?php echo __('No'); ?></option>
                                        </select>
                                    </td>
                                    <td class="comment one-third">
                                        For future use, set to No for now.
                                    </td>
                                </tr>
                            </table>

                        </div> <!-- page -->
                        <div id="github-settings" class="page">
                            <?php
                            /**
                             * GITHUB
                             */
                            ?>
                            <p>
                                To use Github Authorization you need to create Application for your website. Go to:
                                <a href="https://github.com/settings/applications">https://github.com/settings/applications</a>
                            </p>
                            <p>
                                There you need to set up your application info.
                                In Application settings set:<br/> 
                                <b>URL</b> to: <samp><?php echo rtrim(URL_PUBLIC, '/'); ?></samp><br/>
                                <b>Callback URL</b> to: <samp><?php echo URL_PUBLIC . SOCIAL_FRONTEND_URI; ?>/github</samp>
                            </p>
                            <p>
                                Then copy <b>Client ID</b> and <b>Client Secret</b> to according fields below.
                            </p>
                            <table class="settings full">
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_github_enabled">
                                            Allow github OAuth?
                                        </label>
                                    </td>
                                    <td class="field">
                                        <select class="full" id="setting_github_enabled" name="settings[github_enabled]">
                                            <option value="1" <?php echo (isset($settings['github_enabled']) && ($settings['github_enabled'] == '1')) ? 'selected="selected"' : ''; ?>><?php echo __('Yes'); ?></option>
                                            <option value="0" <?php echo (isset($settings['github_enabled']) && ($settings['github_enabled'] == '0')) ? 'selected="selected"' : ''; ?>><?php echo __('No'); ?></option>
                                        </select>
                                    </td>
                                    <td class="comment one-third">
                                        Client ID
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_github_client_id">
                                            Client ID
                                        </label>
                                    </td>
                                    <td class="field">
                                        <input class="full" id="setting_github_client_id" name="settings[github_client_id]" type="text" value="<?php echo isset($settings['github_client_id']) ? $settings['github_client_id'] : ''; ?>" />
                                    </td>
                                    <td class="comment one-third">
                                        Client ID
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_github_client_secret">
                                            Client Secret
                                        </label>
                                    </td>
                                    <td class="field">
                                        <input class="full" id="setting_github_client_secret" name="settings[github_client_secret]" type="text" value="<?php echo isset($settings['github_client_secret']) ? $settings['github_client_secret'] : ''; ?>" />
                                    </td>
                                    <td class="comment one-third">
                                        Client Secret
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_github_scope">
                                            Scope
                                        </label>
                                    </td>
                                    <td class="field">
                                        <input class="full" id="setting_github_scope" name="settings[github_scope]" type="text" value="<?php echo isset($settings['github_scope']) ? $settings['github_scope'] : ''; ?>" />
                                    </td>
                                    <td class="comment one-third">
                                        Separate with <b>commas - ","</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label wide">
                                        <label for="setting_github_offline">
                                            Offline access
                                        </label>
                                    </td>
                                    <td class="field">
                                        <select class="full" id="setting_github_offline" name="settings[github_offline]">
                                            <option value="1" <?php echo (isset($settings['github_offline']) && ($settings['github_offline'] == '1')) ? 'selected="selected"' : ''; ?>><?php echo __('Yes'); ?></option>
                                            <option value="0" <?php echo (isset($settings['github_offline']) && ($settings['github_offline'] == '0')) ? 'selected="selected"' : ''; ?>><?php echo __('No'); ?></option>
                                        </select>
                                    </td>
                                    <td class="comment one-third">
                                        For future use, set to No for now.
                                    </td>
                                </tr>
                            </table>
                            <p>
                                <b>Scope</b> is a list of information your website will ask user to provide. For Github default is:
                            </p>
                            <p>
                                <textarea>user:email</textarea>
                            </p>

                        </div> <!-- page -->

                    </div> <!-- pages -->
                </div>

                <p class="buttons">
                    <input class="button" name="commit" type="submit" value="<?php echo __('Save'); ?>" />
                </p>
            </div>
        </form>
    </div>
<?php endif; ?>

<script type="text/javascript">
    // <![CDATA[
    function setConfirmUnload(on, msg) {
        window.onbeforeunload = (on) ? unloadMessage : null;
        return true;
    }

    function unloadMessage() {
        return '<?php echo __('You have modified this page.  If you navigate away from this page without first saving your data, the changes will be lost.'); ?>';
    }

    $(document).ready(function() {
        // Setup tabs
        var tabContainers = $('div.tabs > div.pages > div');

        $('div.tabs ul.tabNavigation a').click(function() {
            tabContainers.hide().filter(this.hash).show();

            $('div.tabs ul.tabNavigation a').removeClass('here');
            $(this).addClass('here');

            return false;
        }).filter(':first').click();


        // Prevent accidentally navigating away
        $(':input').bind('change', function() {
            setConfirmUnload(true);
        });
        $('form').submit(function() {
            setConfirmUnload(false);
            return true;
        });
    });


    // ]]>
</script>