<?php
/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') ) {
    exit();
}
?>
<h1><?php echo __('Pending email registrations'); ?></h1>
<?php Observer::notify('mm_core_stylesheet'); ?>
<style>
    .pending-delete, .pending-accept { opacity: .3; cursor: pointer;}
    .pending-delete:hover, .pending-accept:hover { opacity: 1;}

    #popupJSON {position: fixed; top:3%; right:3%; bottom: 3%; left: 3%; background-color: #EEE; display:none; 
                -webkit-box-shadow: 2px 2px 30px rgba(50, 50, 50, 0.7) !important;
                -moz-box-shadow:    2px 2px 30px rgba(50, 50, 50, 0.7) !important;
                box-shadow:         2px 2px 30px rgba(50, 50, 50, 0.7) !important;
                z-index: 9999;

    }
    #preJSON { border: none; outline: none; }
    #preJSONtitle { font-size: 18px; background-color: #222; color: #CCC; padding: 8px 12px; height: 24px;}
    #preJSONclose { position: absolute; top: 8px; right: 12px; color: #CCC; cursor: pointer;}
    #preJSONwrapper { position: absolute; top:40px; right:0px; bottom: 0px; left: 0px; overflow-y: auto; }

    #identities-list tr:nth-child(odd) {background-color: #f8f8f8;}
    #identities-list tr:hover {background-color: #eee;}
    #identities-list td {padding: 0px 1px;}
    #identities-list td.center {text-align: center;}
    #identities-list td.right {text-align: right;}
    #identities-list td.id {width: 50px;color:#AAA;}

    table.tablesorter thead tr th, table.tablesorter tfoot tr th { background-color: #eee; border: 1px solid #ccc;font-size: 12px;padding: 4px;}
    table.tablesorter thead tr .header { background-image: url(<?php echo PLUGINS_URI . 'social_login/icons/bg.gif'; ?>); background-repeat: no-repeat; background-position: center right; cursor: pointer; padding-right: 20px;}
    table.tablesorter thead tr .headerSortUp { background-image: url(<?php echo PLUGINS_URI . 'social_login/icons/asc.gif'; ?>); }
    table.tablesorter thead tr .headerSortDown { background-image: url(<?php echo PLUGINS_URI . 'social_login/icons/desc.gif'; ?>); }
    table.tablesorter thead tr .headerSortDown, table.tablesorter thead tr .headerSortUp { background-color: #ccc; }

    div.tablesorterPager { background-color: #eee; border: 1px solid #ccc; text-align: right; }
    div.tablesorterPager span { padding: 0px 5px; }
    div.tablesorterPager img { position: relative; top: 3px; opacity: .5; cursor: pointer;}
    div.tablesorterPager img:hover { opacity: 1;}
    div.tablesorterPager input { width: 50px; text-align: center; }

    pre {outline: 1px solid #ccc; padding: 5px; margin: 5px; }
    .string { color: green; }
    .number { color: darkorange; }
    .boolean { color: blue; }
    .null { color: magenta; }
    .key { color: red; }
</style>
<div id="mm_plugin">
    <table id="pending-list" class="full tablesorter">
        <thead>

        <th><?php echo __('ID'); ?></th>
        <th><?php echo __('Registration date'); ?></th>
        <th><?php echo __('Username'); ?></th>
        <th><?php echo __('Name'); ?></th>
        <th><?php echo __('Email'); ?></th>
        <th><?php echo __('Email in service'); ?></th>
        <th><?php echo __('Actions'); ?></th>
        </thead>
        <tbody>
            <?php foreach ( $userPending as $item ): ?>
                <tr>
                    <td>
                        <?php echo $item->id; ?>
                    </td>
                    <td>
                        <?php echo $item->reg_date; ?>
                    </td>
                    <td>
                        <?php echo $item->name; ?>
                    </td>
                    <td>
                        <?php echo $item->username; ?>
                    </td>
                    <td>
                        <?php echo $item->email; ?>
                    </td>
                    <td class="right">
                        <?php echo $item->reg_hash; ?>
                    </td>
                    <td class="right">
                        <img class="pending-delete" data-id="<?php echo $item->id; ?>" src="<?php echo PLUGINS_URI; ?>social_login/icons/cross.png" class="first" title="<?php echo __('Delete pending registration'); ?>">
                        <img class="pending-accept" data-id="<?php echo $item->id; ?>" src="<?php echo PLUGINS_URI; ?>social_login/icons/check.png" class="first" title="<?php echo __('Register user'); ?>">
                        <div style="display:none;" id="data-provided-<?php echo $item->id; ?>"><?php echo $item->json; ?></div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div id="pager" class="tablesorterPager full">
        <form>
            <img src="<?php echo PLUGINS_URI; ?>social_login/icons/arrow-left-double.png" class="first">
            <img src="<?php echo PLUGINS_URI; ?>social_login/icons/arrow-left.png" class="prev">
            <input type="text" class="pagedisplay">
            <img src="<?php echo PLUGINS_URI; ?>social_login/icons/arrow-right.png" class="next">
            <img src="<?php echo PLUGINS_URI; ?>social_login/icons/arrow-right-double.png" class="last">
            <select class="pagesize">
                <option value="10">10</option>
                <option selected="selected" value="20">20</option>
                <option value="30">30</option>
                <option value="40">40</option>
            </select>
        </form>
    </div>
    <p>
        <?php echo __('The list above shows users who filled up registration form but haven\'t yet confirmed their account.'); ?>
    </p>
</div>


<script>
            $(document).delegate('#service_filter, #userid_filter', 'change', function(e) {
                var sfilter = ($('#service_filter').val() !== '') ? 'service_filter=' + $('#service_filter').val() : '';
                var ufilter = ($('#userid_filter').val() !== '') ? 'userid_filter=' + $('#userid_filter').val() : '';

                document.location = '<?php echo get_url('plugin/social_login/index/'); ?>?'
                        + sfilter
                        + ((ufilter.length > 0) ? '&' : '')
                        + ufilter;
            });

            $(document).ready(function()
            {
                $("#identities-list").tablesorter({
                    headers: {
                        0: {
                            sorter: false
                        },
                        6: {
                            sorter: false
                        }
                    }
                }).tablesorterPager({
                    container: $("#pager"),
                    positionFixed: false,
                    size: 20
                });
            }
            );

            $(document).delegate('.pending-delete', 'click', function(e) {
                var item = $(this);
                if (e.shiftKey || confirm('<?php echo __('Are you sure you want to delete it?'); ?>' + '\n' + '\n' + '<?php echo __('Note: You can hold SHIFT to delete instanty'); ?>')) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo get_url('plugin/social_login/pending_delete/'); ?>",
                        dataType: 'json',
                        data: {"id": item.attr('data-id')
                        },
                        success: function(json) {
                            mmShowMessage(json);
                            item.parents('tr').fadeOut('fast', function() {
                                item.parents('tr').remove();
                            });
                            //location.reload();
                        }
                    });
                }
            });

</script>