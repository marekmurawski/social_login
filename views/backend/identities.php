<?php
/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') ) {
    exit();
}
?>
<h1><?php echo __('Social identities'); ?></h1>
<?php Observer::notify('mm_core_stylesheet'); ?>
<style>
    .see-details, .identity-delete { opacity: .3; cursor: pointer;}
    .see-details:hover, .identity-delete:hover { opacity: 1;}

    #popupJSON {position: fixed; top:3%; right:3%; bottom: 3%; left: 3%; background-color: #EEE; display:none; 
                -webkit-box-shadow: 2px 2px 30px rgba(50, 50, 50, 0.7) !important;
                -moz-box-shadow:    2px 2px 30px rgba(50, 50, 50, 0.7) !important;
                box-shadow:         2px 2px 30px rgba(50, 50, 50, 0.7) !important;
                z-index: 9999;

    }
    #preJSON { border: none; outline: none; }
    #preJSONtitle { font-size: 18px; background-color: #222; color: #CCC; padding: 8px 12px; height: 24px;}
    #preJSONclose { position: absolute; top: 8px; right: 12px; color: #CCC; cursor: pointer;}
    #preJSONwrapper { position: absolute; top:40px; right:0px; bottom: 0px; left: 0px; overflow-y: auto; }

    #identities-list tr:nth-child(odd) {background-color: #f8f8f8;}
    #identities-list tr:hover {background-color: #eee;}
    #identities-list td {padding: 0px 1px;}
    #identities-list td.center {text-align: center;}
    #identities-list td.right {text-align: right;}
    #identities-list td.id {width: 50px;color:#AAA;}

    table.tablesorter thead tr th, table.tablesorter tfoot tr th { background-color: #eee; border: 1px solid #ccc;font-size: 12px;padding: 4px;}
    table.tablesorter thead tr .header { background-image: url(<?php echo PLUGINS_URI . 'social_login/icons/bg.gif'; ?>); background-repeat: no-repeat; background-position: center right; cursor: pointer; padding-right: 20px;}
    table.tablesorter thead tr .headerSortUp { background-image: url(<?php echo PLUGINS_URI . 'social_login/icons/asc.gif'; ?>); }
    table.tablesorter thead tr .headerSortDown { background-image: url(<?php echo PLUGINS_URI . 'social_login/icons/desc.gif'; ?>); }
    table.tablesorter thead tr .headerSortDown, table.tablesorter thead tr .headerSortUp { background-color: #ccc; }

    div.tablesorterPager { background-color: #eee; border: 1px solid #ccc; text-align: right; }
    div.tablesorterPager span { padding: 0px 5px; }
    div.tablesorterPager img { position: relative; top: 3px; opacity: .5; cursor: pointer;}
    div.tablesorterPager img:hover { opacity: 1;}
    div.tablesorterPager input { width: 50px; text-align: center; }

    pre {outline: 1px solid #ccc; padding: 5px; margin: 5px; }
    .string { color: green; }
    .number { color: darkorange; }
    .boolean { color: blue; }
    .null { color: magenta; }
    .key { color: red; }
</style>
<div id="mm_plugin">
    <table class="header full">
        <tr>
            <td class="label wide">
                <label><?php echo __('Filter by service'); ?></label>
            </td>
            <td>
                <select id="service_filter">
                    <option value=""><?php echo __('do not filter'); ?></option>
                    <option value="">---</option>
                    <?php foreach ( SocialLoginSettings::$services as $serviceKey => $service ): ?>
                        <option value="<?php echo $serviceKey ?>"<?php if ( $service_filter == $serviceKey ) echo ' selected="selected"'; ?>><?php echo $serviceKey ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
            <td class="label wide">
                <label><?php echo __('Filter by Username'); ?></label>
            </td>
            <td>
                <select id="userid_filter">
                    <option value=""><?php echo __('do not filter'); ?></option>
                    <option value="">---</option>
                    <?php foreach ( $users as $user ): ?>
                        <option value="<?php echo $user->id ?>"<?php if ( $userid_filter == $user->id ) echo ' selected="selected"'; ?>><?php echo $user->username . ' (' . $user->name . ')'; ?></option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
    </table>
    <table id="identities-list" class="full tablesorter">
        <thead>

        <th><?php echo __('Logo'); ?></th>
        <th><?php echo __('Service type'); ?></th>
        <th><?php echo __('Offline access'); ?></th>
        <th><?php echo __('ID in service'); ?></th>
        <th><?php echo __('Name in service'); ?></th>
        <th><?php echo __('Email in service'); ?></th>
        <th><?php echo __('Username'); ?></th>
        <th><?php echo __('Data provided'); ?></th>
        </thead>
        <tbody>
            <?php foreach ( $socialAccounts as $item ): ?>
                <tr>
                    <td class="center">
                        <?php
                        $image = '<img src="' . PLUGINS_URI . 'social_login/icons/services/' . $item->service_type . '.png" alt="' . $item->service_type . '" title="' . $item->service_type . ', id=' . $item->id . '"/>';
                        echo $image;
                        ?>
                    </td>
                    <td>
                        <?php echo $item->service_type; ?>
                    </td>
                    <td>
                        <?php echo !empty($item->refresh_token) ? __('Yes') : __('No'); ?>
                    </td>
                    <td>
                        <?php echo $item->social_id; ?>
                    </td>
                    <td>
                        <?php echo $item->social_name; ?>
                    </td>
                    <td>
                        <?php echo $item->social_email; ?>
                    </td>
                    <td class="right">
                        <?php echo $item->user_username; ?>
                    </td>
                    <td class="right">
                        <img class="identity-delete" data-id="<?php echo $item->id; ?>" src="<?php echo PLUGINS_URI; ?>social_login/icons/cross.png" class="first" title="<?php echo __('Delete identity'); ?>">
                        <img class="see-details" data-id="<?php echo $item->id; ?>" src="<?php echo PLUGINS_URI; ?>social_login/icons/zoom.png" class="first" title="<?php echo __('See provided JSON data'); ?>">
                        <div style="display:none;" id="data-provided-<?php echo $item->id; ?>"><?php echo $item->json; ?></div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div id="pager" class="tablesorterPager full">
        <form>
            <img src="<?php echo PLUGINS_URI; ?>social_login/icons/arrow-left-double.png" class="first">
            <img src="<?php echo PLUGINS_URI; ?>social_login/icons/arrow-left.png" class="prev">
            <input type="text" class="pagedisplay">
            <img src="<?php echo PLUGINS_URI; ?>social_login/icons/arrow-right.png" class="next">
            <img src="<?php echo PLUGINS_URI; ?>social_login/icons/arrow-right-double.png" class="last">
            <select class="pagesize">
                <option value="10">10</option>
                <option selected="selected" value="20">20</option>
                <option value="30">30</option>
                <option value="40">40</option>
            </select>
        </form>
    </div>
    <p>
        <?php echo __('To connect or disconnect services create new Wolf page and set it\'s behavior to <b>Social profile</b>. Then go to this page in frontend.'); ?>
    </p>
    <div id="popupJSON">
        <div id="preJSONtitle"><?php echo __('JSON data preview'); ?></div>
        <img id="preJSONclose" src="<?php echo PLUGINS_URI; ?>social_login/icons/cross.png" onclick="$(this).parent().fadeOut('fast');" title="<?php echo __('Close'); ?>" />
        <div id="preJSONwrapper">
            <pre id="preJSON"></pre>
        </div>
    </div>
</div>


<script>
            $(document).delegate('#service_filter, #userid_filter', 'change', function(e) {
                var sfilter = ($('#service_filter').val() !== '') ? 'service_filter=' + $('#service_filter').val() : '';
                var ufilter = ($('#userid_filter').val() !== '') ? 'userid_filter=' + $('#userid_filter').val() : '';

                document.location = '<?php echo get_url('plugin/social_login/index/'); ?>?'
                        + sfilter
                        + ((ufilter.length > 0) ? '&' : '')
                        + ufilter;
            });

            $(document).ready(function()
            {
                $("#identities-list").tablesorter({
                    headers: {
                        0: {
                            sorter: false
                        },
                        7: {
                            sorter: false
                        }
                    }
                }).tablesorterPager({
                    container: $("#pager"),
                    positionFixed: false,
                    size: 20
                });
            }
            );
            // http://stackoverflow.com/questions/4810841/json-pretty-print-using-javascript
            function syntaxHighlight(json) {
                json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
                return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function(match) {
                    var cls = 'number';
                    if (/^"/.test(match)) {
                        if (/:$/.test(match)) {
                            cls = 'key';
                        } else {
                            cls = 'string';
                        }
                    } else if (/true|false/.test(match)) {
                        cls = 'boolean';
                    } else if (/null/.test(match)) {
                        cls = 'null';
                    }
                    return '<span class="' + cls + '">' + match + '</span>';
                });
            }

            $(document).delegate('.identity-delete', 'click', function(e) {
                var item = $(this);
                if (e.shiftKey || confirm('<?php echo __('Are you sure you want to delete it?'); ?>' + '\n' + '\n' + '<?php echo __('Note: You can hold SHIFT to delete instanty'); ?>')) {
                    $.ajax({
                        type: "POST",
                        url: "<?php echo get_url('plugin/social_login/identity_delete/'); ?>",
                        dataType: 'json',
                        data: {"id": item.attr('data-id')
                        },
                        success: function(json) {
                            mmShowMessage(json);
                            item.parents('tr').fadeOut('fast', function() {
                                item.parents('tr').remove();
                            });
                            //location.reload();
                        }
                    });
                }
            });

            $(document).delegate('.see-details', 'click', function(e) {
                //var data = '{"dsd":"dddd"}';
                var identity = $(this).attr('data-id');
                //alert(identity);
                var data = $('#data-provided-' + identity).html();
                //alert(data);
                var interpretedData = JSON.parse(data);

                var str = JSON.stringify(interpretedData, undefined, 4);
                $('#preJSON').html(syntaxHighlight(str));
                $('#popupJSON').fadeIn('fast');
                // document.body.appendChild(document.createElement('pre')).innerHTML = syntaxHighlight(str);
            });

</script>