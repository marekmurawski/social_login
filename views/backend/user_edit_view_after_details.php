<?php
/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') ) {
    exit();
}
?>
<h3>
    <?php echo __('Social identities associated with this user'); ?>
</h3>
<table style="width: 100%" class="index">
    <thead>
    <th><?php echo __('Logo'); ?></th>
    <th><?php echo __('ID'); ?></th>
    <th><?php echo __('Service type'); ?></th>
    <th><?php echo __('ID in service'); ?></th>
    <th><?php echo __('Name in service'); ?></th>
    <th><?php echo __('Email in service'); ?></th>
</thead>
<tbody>
    <?php foreach ( $socialAccounts as $item ): ?>
        <tr>
            <td>
                <?php
                $image = '<img src="' . PLUGINS_URI . 'social_login/icons/services/' . $item->service_type . '.png" alt="' . $item->service_type . '"/>';
                echo $image;
                ?>
            </td>
            <td>
                <?php echo $item->id; ?>
            </td>
            <td>
                <?php echo $item->service_type; ?>
            </td>
            <td>
                <?php echo $item->social_id; ?>
            </td>
            <td>
                <?php echo $item->social_name; ?>
            </td>
            <td>
                <?php echo $item->social_email; ?>
            </td>
        </tr>
    <?php endforeach; ?>
</tbody>
</table>
<hr/>
<p>
    <?php echo __('To connect or disconnect services create new Wolf page and set it\'s behavior to <b>Social profile</b>. Then go to this page in frontend.'); ?>
</p>