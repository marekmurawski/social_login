<?php
/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') ) {
    exit();
}
?>
<h1><?php echo __('Documentation');
?></h1>
<p>
    Not ready yet
</p>
<p>
    See <b>readme.MD</b>
</p>

