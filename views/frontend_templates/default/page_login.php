<?php
/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') ) {
    exit();
}

$formErrors = Flash::get(SOCIAL_FLASH_FORM_ERROR);

$login_required = Flash::get('login_required');

/**
 * Default styles
 */
if ( $settings['default_css'] ) {
    $csspath = PLUGINS_ROOT . '/social_login/views/frontend_templates/' . $settings['template'] . '/page.css';
    if ( file_exists($csspath) ) {
        $cssfile = file_get_contents($csspath);
        print ($cssfile) ? '<style>' . PHP_EOL . $cssfile . '</style>' . PHP_EOL : '';
    }
}
?>


<div id="social_login_page">
    <?php if ( null !== $login_required ): ?>
        <?php
        // Store requested URL for later back-redirecting 
        Social::setLoginRequiredUrl($login_required);
        ?>
        <div class="notice-text">
            <p><?php echo __('You requested to access'); ?></p>
            <p><a href="<?php echo $login_required; ?>"><?php echo $login_required; ?></a></p>
            <p>
                <?php echo __('This page is only accessible for logged in users.'); ?>
                <?php echo __('You can login here using your username and password.'); ?>
            </p>
        </div>
    <?php endif; ?> 
    <?php
    /**
     * Alert boxes
     */
    if ( (bool) $settings['show_alerts'] ) {
        $view = new View(SOCIAL_VIEW_FOLDER . 'frontend_templates/' . $settings['template'] . '/alerts');
        echo $view;
    }
    ?>
    <div class="box-wrapper centered">
        <div class="box-title">
            <?php echo __('Login'); ?>
        </div>
        <div class="box-content">
            <form action="<?php echo URL_PUBLIC . $profile_page_uri . '/' . $settings['slug_login']; ?>" method="POST">
                <table>
                    <tr>
                        <td class="social-label">
                            <label for="login_username">
                                <?php echo __('Username'); ?>
                            </label>
                        </td>
                        <td class="field">
                            <input type="text" 
                            <?php echo (!empty($formErrors['username'])) ? 'class="error"' : ''; ?> 
                                   placeholder="<?php echo ( AuthUser::ALLOW_LOGIN_WITH_EMAIL ) ? __('your username or email') : __('your username'); ?>" 
                                   id="login_username" 
                                   name="login[username]" 
                                   value="<?php echo (!empty($formValues['username']) ? $formValues['username'] : ''); ?>" />
                                   <?php if ( !empty($formErrors['username']) ): ?>
                                <div class="form-item-errors">
                                    <?php foreach ( $formErrors['username'] as $error ): ?>
                                        <div><?php echo $error; ?></div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                        </td>
                        <td class="help medium">
                            <?php
                            if ( AuthUser::ALLOW_LOGIN_WITH_EMAIL ) {
                                echo __('Type your username or email');
                            } else {
                                echo __('Type your username');
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="social-label">
                            <label for="login_password">
                                <?php echo __('Password'); ?>
                            </label>
                        </td>
                        <td class="field">
                            <input type="password" 
                                   placeholder="<?php echo __('your password'); ?>" 
                                   id="login_password" 
                                   name="login[password]" 
                                   value="" />
                        </td>
                        <td class="help medium">
                            <?php echo __('Password'); ?>
                        </td>
                    </tr>
                </table>
                <div class="actions">
                    <button class="button" name="commit" type="submit" ><?php echo __('Login'); ?></button>
                    <?php if ( (bool) SocialLoginSettings::get('email_enabled') ): ?>
                        <?php echo __('or'); ?>
                        <a class="button" rel="nofollow" href="<?php echo URL_PUBLIC . $profile_page_uri . '/' . $settings['slug_register']; ?>">
                            <?php echo __('Register new account'); ?>
                        </a>
                    <?php endif; ?>                    
                    <br/>
                    (<a href="<?php echo URL_PUBLIC . $profile_page_uri . '/' . $settings['slug_forgot']; ?>"><?php echo __('Forgot password?'); ?></a>)
                </div>
            </form>
        </div>        
    </div>
    <div class="box-wrapper centered">
        <div class="box-title">
            <?php echo __('Social login'); ?>
        </div>
        <div class="box-content">
            <?php if ( $valid_services ): ?>
                <div class='box-comment'>
                    <p><?php echo __('You can also use one of the following social services to login:'); ?></p>
                </div>
                <div class="actions">
                    <?php foreach ( $valid_services as $service ): ?>
                        <a class="social-icon" rel="nofollow" href="<?php echo URL_PUBLIC . SOCIAL_FRONTEND_URI . '/' . $service; ?>">
                            <img src="<?php echo trim($settings['icons_uri'], '/\\') . '/' . $service; ?>.png" alt="<?php echo $service . ' ' . __('login'); ?>" />
                            <span><?php echo ucfirst($service); ?></span>
                        </a>
                    <?php endforeach; ?>
                </div>
                <div class='box-comment'>
                    <p>
                        <?php echo __('Social login process uses <b>external service authority</b> to authenticate you.'); ?>
                        <?php echo __('You will be redirected to your social service <b>login page</b> and asked to <b>grant permission</b> for this site.'); ?>
                    </p>
                    <p>
                        <?php echo __('When you successfully authenticate, we will create account for you and automatically log you in.'); ?>
                    </p>
                </div>
            <?php else: ?>
                <div class='box-comment'>
                    <p>
                        <?php echo __('There are no social services configured for login.'); ?>
                    </p>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>