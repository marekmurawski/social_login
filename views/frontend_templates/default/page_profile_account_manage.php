<?php
/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') ) {
    exit();
}

//$settings['edit_username'] = (isset($settings['edit_username'])) ? (bool) $settings['edit_username'] : true;
//$settings['edit_email']    = (isset($settings['edit_email'])) ? (bool) $settings['edit_email'] : true;
//$settings['edit_password'] = (isset($settings['edit_password'])) ? (bool) $settings['edit_password'] : true;

$formErrors = Flash::get(SOCIAL_FLASH_FORM_ERROR);

// echo '<pre>' . print_r($formValues, true) . '</pre>';
?>
<form action="<?php echo URL_PUBLIC . $profile_page_uri . '/save'; ?>" method="post">
    <div class="box-wrapper">
        <div class="box-title">
            <?php echo __('Account Settings'); ?>
        </div>
        <div class="box-content">
            <table>
                <tr>
                    <td class="social-label">
                        <label for="user_name"><?php echo __('Your name'); ?></label>
                    </td>
                    <td class="field">
                        <input id="user_name" 
                        <?php echo (!empty($formErrors['name'])) ? 'class="error"' : ''; ?>
                               maxlength="100" 
                               name="user[name]" 
                               type="text" 
                               value="<?php echo isset($formValues['name']) ? $formValues['name'] : $user->name; ?>" />
                               <?php if ( !empty($formErrors['name']) ): ?>
                            <div class="form-item-errors">
                                <?php foreach ( $formErrors['name'] as $error ): ?>
                                    <div><?php echo $error; ?></div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>                        
                    </td>
                    <td class="help wide">
                        <?php
                        echo __('At least :num characters.', array(
                                    ':num' => SocialLoginSettings::get('min_username_length'),
                        ));
                        ?>
                    </td>
                </tr>
                <?php if ( $settings['edit_username'] ): ?>
                    <tr>
                        <td class="social-label">
                            <label for="user_username"><?php echo __('Username'); ?></label>
                        </td>
                        <td class="field">
                            <input id="user_username" 
                            <?php echo AuthUser::getId() == 1 ? 'disabled="disabled" ' : ''; ?>
                            <?php echo (!empty($formErrors['username'])) ? 'class="error"' : ''; ?>
                                   maxlength="40" 
                                   name="user[username]" 
                                   type="text" 
                                   value="<?php echo isset($formValues['username']) ? $formValues['username'] : $user->username; ?>" />
                                   <?php if ( !empty($formErrors['username']) ): ?>
                                <div class="form-item-errors">
                                    <?php foreach ( $formErrors['username'] as $error ): ?>
                                        <div><?php echo $error; ?></div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>                              
                        </td>
                        <td class="help wide">
                            <?php
                            echo __('At least :num characters.', array(
                                        ':num' => SocialLoginSettings::get('min_username_length'),
                            ));
                            ?>
                        </td>
                    </tr>
                <?php endif; ?>
                <?php if ( $settings['edit_email'] ): ?>
                    <tr>
                        <td class="social-label">
                            <label class="optional" for="user_email"><?php echo __('E-mail'); ?></label>
                        </td>
                        <td class="field">
                            <input id="user_email" 
                            <?php echo (!empty($formErrors['email'])) ? 'class="error"' : ''; ?>
                                   maxlength="255" 
                                   name="user[email]" 
                                   type="text" 
                                   value="<?php echo isset($formValues['email']) ? $formValues['email'] : $user->email ?>" />
                                   <?php if ( !empty($formErrors['email']) ): ?>
                                <div class="form-item-errors">
                                    <?php foreach ( $formErrors['email'] as $error ): ?>
                                        <div><?php echo $error; ?></div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>                                 
                        </td>
                        <td class="help wide">
                            <?php echo __('Optional. If set, should be valid.'); ?>
                        </td>
                    </tr>
                <?php endif; ?>
            </table>
            <?php if ( $settings['edit_password'] ): ?>
                <?php if ( strlen($user->password) < 1 ): ?>
                    <div class="notice-text">
                        <p>
                            <?php
                            echo __('Note: <b>Your password is empty</b>. It seems your account was created using social login.');
                            ?>
                        </p>
                        <p>
                            <?php
                            echo __('Set your new password to be able to login using username <b>:username</b> and your new password.', array(
                                        ':username' => $user->username
                            ));
                            ?>
                        </p>
                        <p>
                            <?php
                            echo __('If you leave your password empty you will only be able to login to this site with social identities attached to your accout.');
                            ?>
                        </p>
                    </div>
                <?php else: ?>
                    <div class="box-comment">
                        <p>
                            <?php echo __('Fill the fields below to change password for your account.'); ?>
                        </p>
                    </div>
                <?php endif; ?>
                <table>
                    <tr>
                        <td class="social-label"><label for="user_password">
                                <?php echo __('New password'); ?></label>
                        </td>
                        <td class="field">
                            <input id="user_password" 
                            <?php echo (!empty($formErrors['password'])) ? 'class="error"' : ''; ?>
                                   maxlength="40" 
                                   name="user[password]" 
                                   type="password" 
                                   value="" />
                                   <?php if ( !empty($formErrors['password']) ): ?>
                                <div class="form-item-errors">
                                    <?php foreach ( $formErrors['password'] as $error ): ?>
                                        <div><?php echo $error; ?></div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>   
                        </td>
                        <td class="help wide" rowspan="2">
                            <?php
                            echo __('At least :num characters.', array(
                                        ':num' => SocialLoginSettings::get('min_password_length'),
                            ));
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="social-label">
                            <label for="user_confirm"><?php echo __('New password confirm'); ?></label>
                        </td>
                        <td class="field">
                            <input id="user_confirm" maxlength="40" name="user[confirm]" type="password" value="" />
                        </td>
                    </tr>
                </table>
            <?php endif; ?>
            <div class="actions">
                <button class="button" name="commit" type="submit" ><?php echo __('Save account settings'); ?></button>

            </div>
        </div>
    </div>
</form>
