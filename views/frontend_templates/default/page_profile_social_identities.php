<?php
/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') ) {
    exit();
}
?>
<div class="box-wrapper">
    <div class="box-title">
        <?php echo __('Social identities'); ?>
    </div>
    <?php if ( $social ): ?>
        <div class="box-content">
            <div class='box-comment'>
                <p>
                    <?php echo __('Your account is <b>connected</b> to the following social identities:'); ?>
                </p>
            </div>
            <table class="identities">
                <thead>
                <th></th>
                <th><?php echo __('Service name'); ?></th>
                <th><?php echo __('Service ID'); ?></th>
                <th><?php echo __('Name in service'); ?></th>
                <th><?php echo __('E-mail in service'); ?></th>
                <th><?php echo __('Actions'); ?></th>
                </thead>
                <tbody>
                    <?php foreach ( $social as $item ): ?>
                        <tr>
                            <td>
                                <img src="<?php echo URL_PUBLIC . trim($settings['icons_uri'],'/\\') .'/'. $item->service_type . '.png'; ?>"  alt="<?php echo $item->service_type; ?>" title="<?php echo $item->service_type; ?>"/>
                            </td>
                            <td>
                                <?php echo $item->service_type; ?>
                            </td>
                            <td>
                                <?php echo $item->social_id; ?>
                            </td>
                            <td>
                                <?php echo $item->social_name; ?>
                            </td>
                            <td>
                                <?php echo $item->social_email; ?>
                            </td>
                            <td>
                                <a href="<?php echo URL_PUBLIC . SOCIAL_FRONTEND_URI . '/' . $item->service_type . '?action=disconnect&id=' . $item->social_id; ?>">
                                    <img src="<?php echo URL_PUBLIC . trim($settings['icons_uri'],'/\\') .'/'. 'disconnect.png'; ?>" title="<?php echo __('Disconnect :service from your account', array(':service' => $item->service_type)); ?>"  alt="<?php echo __('Disconnect :service from your account', array(':service' => $service)); ?>"/>
                                </a>
                            </td>

                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <div class="box-comment">
                <p>
                    <?php echo __('Your account is <b>not connected</b> to any social identity.'); ?>
                </p>
            </div>
        <?php endif; ?>
        <?php if ( $valid_services = SocialLoginSettings::getValidServices() ): ?>
            <div class="box-comment">
                <p>
                    <?php echo __('You can <b>connect your account</b> with the following social identities:'); ?>
                </p>
            </div>
            <div class="actions">
                <?php foreach ( $valid_services as $service ): ?>
                    <a class="social-icon" rel="nofollow" href="<?php echo URL_PUBLIC . SOCIAL_FRONTEND_URI . '/' . $service . '?action=connect'; ?>">
                        <img src="<?php echo URL_PUBLIC . trim($settings['icons_uri'],'/\\') .'/'. $service; ?>.png" title="<?php echo __('Connect :service to your account', array(':service' => $service)); ?>"  alt="<?php echo __('Connect :service to your account', array(':service' => $service)); ?>"/>
                        <span><?php echo ucfirst($service); ?></span>
                    </a>
                <?php endforeach; ?>
            </div>
            
        <?php else: ?>
            <div class="box-comment">
                <p>
                        <?php echo __('There are no social services configured for connecting.'); ?>
                </p>
            </div>
        <?php endif; ?>
    </div>
</div>