<?php
/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') )
    exit();

// $settings['show_avatar']       = (isset($settings['show_avatar'])) ? (bool) $settings['show_avatar'] : true;
// $settings['avatar_size']       = (isset($settings['avatar_size'])) ? (int) $settings['avatar_size'] : 128;
// $settings['show_created_on']   = (isset($settings['show_created_on'])) ? (bool) $settings['show_created_on'] : true;
// $settings['show_last_login']   = (isset($settings['show_last_login'])) ? (bool) $settings['show_last_login'] : true;
// $settings['show_last_failure'] = (isset($settings['show_last_failure'])) ? (bool) $settings['show_last_failure'] : true;
// $settings['show_username']     = (isset($settings['show_username'])) ? (bool) $settings['show_username'] : true;
// $settings['show_roles']        = (isset($settings['show_roles'])) ? (bool) $settings['show_roles'] : true;
// $settings['show_permissions']  = (isset($settings['show_permissions'])) ? (bool) $settings['show_permissions'] : true;
// $settings['show_actions']      = (isset($settings['show_actions'])) ? (bool) $settings['show_actions'] : true;
?>
<div class="box-wrapper">
    <div class="box-title">
        <?php echo __('Your account information'); ?>
    </div>
    <div class="box-content">
        <?php if ( $settings['show_avatar'] ): ?>
            <img class="avatar" src="http://www.gravatar.com/avatar.php?gravatar_id=<?php echo md5($user->email); ?>&amp;size=<?php echo (int) $settings['avatar_size']; ?>" alt="<?php echo __('Your avatar image'); ?>" title="<?php echo __('Avatar images are associated to your e-mail address via gravatar.com'); ?>"/>
        <?php endif; ?>
        <table>
            <?php if ( $settings['show_created_on'] ): ?>
                <tr>
                    <td class="social-label">
                        <?php echo __('Account created on'); ?>
                    </td>
                    <td class="field strong">
                        <?php echo (!empty($user->created_on)) ? strftime($settings['datetime_format'] , strtotime($user->created_on)) : ''; ?>
                    </td>
                    <td <?php echo ($settings['show_avatar']) ? 'style="width:' . (int) $settings['avatar_size'] . 'px"' : ''; ?>>
                    </td>
                </tr>
            <?php endif; ?>
            <?php if ( $settings['show_last_login'] ): ?>
                <tr>
                    <td class="social-label">
                        <?php echo __('Last login'); ?>
                    </td>
                    <td class="field strong">
                        <?php echo (!empty($user->last_login)) ? strftime($settings['datetime_format'] , strtotime($user->last_login)) : ''; ?>
                    </td>
                    <td <?php echo ($settings['show_avatar']) ? 'style="width:' . (int) $settings['avatar_size'] . 'px"' : ''; ?>>
                    </td>
                </tr>
            <?php endif; ?>
            <?php if ( $settings['show_last_failure'] ): ?>
                <tr>
                    <td class="social-label">
                        <?php echo __('Last failed login'); ?>
                    </td>
                    <td class="field strong">
                        <?php echo (!empty($user->last_failure)) ? strftime($settings['datetime_format'] , strtotime($user->last_failure)) : ''; ?>
                    </td>
                    <td <?php echo ($settings['show_avatar']) ? 'style="width:' . (int) $settings['avatar_size'] . 'px"' : ''; ?>>
                    </td>
                </tr>
            <?php endif; ?>
            <?php if ( $settings['show_username'] ): ?>
                <tr>
                    <td class="social-label">
                        <?php echo __('Your username'); ?>
                    </td>
                    <td class="field strong">
                        <?php echo $user->username; ?>
                    </td>
                    <td <?php echo ($settings['show_avatar']) ? 'style="width:' . (int) $settings['avatar_size'] . 'px"' : ''; ?>>
                    </td>
                </tr>
            <?php endif; ?>
            <?php if ( $settings['show_roles'] ): ?>
                <tr>
                    <td class="social-label">
                        <?php echo __('Your roles'); ?>
                    </td>
                    <td class="field">
                        <?php
//                        $delim = false;
                        if ( $roles = Role::findByUserId($user->id) ) {
                            foreach ( $roles as $role ) {
//                                echo $delim;
                                echo '<span class="role_item">' . Inflector::humanize($role) . '</span>';
//                                if ( !$delim )
//                                    $delim = ', ';
                            }
                        } else echo __('-no roles assigned-');
                        ?>
                    </td>
                    <td <?php echo ($settings['show_avatar']) ? 'style="width:' . (int) $settings['avatar_size'] . 'px"' : ''; ?>>
                    </td>
                </tr>
            <?php endif; ?>
            <?php if ( $settings['show_permissions'] ): ?>
                <tr>
                    <td class="social-label">
                        <?php echo __('Your permissions'); ?>
                    </td>
                    <td class="field">
                        <?php
//                        $delim = false;
                        foreach ( AuthUser::getPermissions() as $role ) {
//                            echo $delim;
                            echo '<span class="permission_item">' . Inflector::humanize($role) . '</span>';
//                            if ( !$delim )
//                                $delim = ', ';
                        }
                        ?>
                    </td>
                    <td <?php echo ($settings['show_avatar']) ? 'style="width:' . (int) $settings['avatar_size'] . 'px"' : ''; ?>>
                    </td>
                </tr>
            <?php endif; ?>
        </table>
        <?php if ( $settings['show_actions'] ): ?>
            <div class="actions">
                <a href="<?php echo URL_PUBLIC . $profile_page_uri . '/' . $settings['slug_logout']; ?>" class="button"><?php echo __('Logout'); ?></a>
                <?php if ( AuthUser::hasPermission('admin_view') ): ?>
                    <a class="button" href="<?php echo URL_PUBLIC . ADMIN_DIR; ?>"><?php echo __('Go to backend'); ?></a>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
</div>