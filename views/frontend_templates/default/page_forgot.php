<?php
/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage members
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') )
    exit();



$formErrors = Flash::get(SOCIAL_FLASH_FORM_ERROR);

/**
 * Default styles
 */
if ( $settings['default_css'] ) {
    $csspath = PLUGINS_ROOT . '/social_login/views/frontend_templates/' . $settings['template'] . '/page.css';
    if ( file_exists($csspath) ) {
        $cssfile = file_get_contents($csspath);
        print ($cssfile) ? '<style>' . PHP_EOL . $cssfile . '</style>' . PHP_EOL : '';
    }
}
?>

<div id="social_login_page">
    <?php
    /**
     * Alert boxes
     */
    if ( (bool) $settings['show_alerts'] ) {
        $view = new View(SOCIAL_VIEW_FOLDER . 'frontend_templates/' . $settings['template'] . '/alerts');
        echo $view;
    }
    ?>
    <div class="box-wrapper centered">
        <div class="box-title">
            <?php echo __('Request password reset'); ?>
        </div>
        <div class="box-content">
            <p>
                <?php echo __('If you forgot your password, you can reset it here by providing your e-mail address.'); ?>
            </p>
            <form action="<?php echo URL_PUBLIC . $profile_page_uri . '/' . $settings['slug_forgot']; ?>" method="post">

                <table>
                    <tr>
                        <td class="social-label">
                            <label for="forgot_email">
                                <?php echo __('E-mail'); ?>
                            </label>
                        </td>
                        <td class="field">
                            <input id="forgot_email" 
                            <?php echo (!empty($formErrors['email'])) ? 'class="error"' : ''; ?> 
                                   placeholder="<?php echo __('your@email.com'); ?>" 
                                   type="text" 
                                   name="forgot[email]" 
                                   value="<?php echo (!empty($formValues['email']) ? $formValues['email'] : ''); ?>" />
                                   <?php if ( !empty($formErrors['email']) ): ?>
                                <div class="form-item-errors">
                                    <?php foreach ( $formErrors['email'] as $error ): ?>
                                        <div><?php echo $error; ?></div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                        </td>
                        <td class="help medium">
                            <?php echo __('Must be valid.'); ?>
                        </td>
                    </tr>
                    <?php
                    /*
                     * CAPTCHA
                     */
                    ?>
                    <tr>
                        <td class="social-label">
                        </td>
                        <td class="field">
                            <a href="#" onclick="document.getElementById('social-login-captcha').src = '<?php echo URL_PUBLIC . SOCIAL_CAPTCHA_URI; ?>?' + Math.random();
                                    return false;">
                                <img id="social-login-captcha" src="<?php echo URL_PUBLIC . SOCIAL_CAPTCHA_URI; ?>" alt="<?php echo __('Security check'); ?>" title="<?php echo __('Unreadable? Click the image to refresh.'); ?>"/>
                            </a>
                        </td>
                        <td class="help medium">
                            <?php echo __('Unreadable? Click the image to refresh.'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="social-label">
                            <label for="<?php echo SOCIAL_CAPTCHA_KEY; ?>">
                                <?php echo __('Security check'); ?>
                            </label>
                        </td>
                        <td class="field">
                            <input type="text" 
                            <?php echo (!empty($formErrors['captcha'])) ? 'class="error"' : ''; ?>
                                   id="<?php echo SOCIAL_CAPTCHA_KEY; ?>" 
                                   validate="true" 
                                   name="<?php echo SOCIAL_CAPTCHA_KEY; ?>" 
                                   value="" />
                        </td>
                        <td class="help medium">
                            <?php echo __('Type the word from the image.'); ?>
                        </td>
                    </tr>
                    <?php
                    /*
                     * END OF CAPTCHA
                     */
                    ?>
                </table>                
                <div class="actions">
                    <button class="button" type="submit" ><?php echo __('Send password'); ?></button>

                    <a class="button" href="<?php echo URL_PUBLIC . $profile_page_uri; ?>"><?php echo __('Go to login page'); ?></a>
                </div>
            </form>
        </div>
    </div>
</div>