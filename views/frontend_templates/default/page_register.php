<?php
/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') )
    exit();

// $settings['template']    = (isset($settings['template'])) ? $settings['template'] : 'default';
// $settings['default_css'] = (isset($settings['default_css'])) ? (bool) $settings['default_css'] : true;


$formErrors = Flash::get(SOCIAL_FLASH_FORM_ERROR);

// print_r($formErrors);

/**
 * Default styles
 */
if ( $settings['default_css'] ) {
    $csspath = PLUGINS_ROOT . '/social_login/views/frontend_templates/' . $settings['template'] . '/page.css';
    if ( file_exists($csspath) ) {
        $cssfile = file_get_contents($csspath);
        print ($cssfile) ? '<style>' . PHP_EOL . $cssfile . '</style>' . PHP_EOL  : '';
    }
}
?>
<div id="social_login_page">
    <?php
    /**
     * Alert boxes
     */
    if ( (bool) $settings['show_alerts'] ) {
        $view = new View(SOCIAL_VIEW_FOLDER . 'frontend_templates/' . $settings['template'] . '/alerts');
        echo $view;
    }
    ?>
    <div class="box-wrapper centered">
        <div class="box-title">
            <?php echo __('Register new account'); ?>
        </div>
        <div class="box-content">
            <div class="box-comment">
                <p>
                    <?php echo __('You can <b>register your new account</b> using email address.'); ?>
                    <?php echo __('We will send you an email with confirmation link to activate your new account.'); ?>
                </p>
            </div>
            <form action="<?php echo URL_PUBLIC . $profile_page_uri . '/' . $settings['slug_register']; ?>" method="POST">
                <table>
                    <tr>
                        <td class="social-label">
                            <label for="register_name">
                                <?php echo __('Name'); ?>
                            </label>
                        </td>
                        <td class="field">
                            <input type="text" 
                            <?php echo (!empty($formErrors['name'])) ? 'class="error"' : ''; ?> 
                                   placeholder="<?php echo __('your name'); ?>" 
                                   id="register_name" 
                                   name="register[name]" 
                                   pattern=".{<?php echo SocialLoginSettings::get('min_username_length'); ?>,}"
                                   title="<?php echo __('At least :num characters.', array( ':num' => SocialLoginSettings::get('min_username_length') )); ?>"
                                   value="<?php echo (!empty($formValues['name']) ? $formValues['name'] : ''); ?>" />
                                   <?php if ( !empty($formErrors['name']) ): ?>
                                <div class="form-item-errors">
                                    <?php foreach ( $formErrors['name'] as $error ): ?>
                                        <div><?php echo $error; ?></div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                        </td>
                        <td class="help medium">
                            <?php
                            echo __('At least :num characters.', array(
                                        ':num' => SocialLoginSettings::get('min_username_length'),
                            ));
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="social-label">
                            <label for="register_username">
                                <?php echo __('Username'); ?>
                            </label>
                        </td>
                        <td class="field">
                            <input type="text" 
                            <?php echo (!empty($formErrors['username'])) ? 'class="error"' : ''; ?> 
                                   placeholder="<?php echo __('your username'); ?>" 
                                   id="register_username" 
                                   name="register[username]"
                                   pattern=".{<?php echo SocialLoginSettings::get('min_username_length'); ?>,}"
                                   validate="true" 
                                   title="<?php echo __('At least :num characters.', array( ':num' => SocialLoginSettings::get('min_username_length') )); ?>"
                                   value="<?php echo (!empty($formValues['username']) ? $formValues['username'] : ''); ?>" />
                                   <?php if ( !empty($formErrors['username']) ): ?>
                                <div class="form-item-errors">
                                    <?php foreach ( $formErrors['username'] as $error ): ?>
                                        <div><?php echo $error; ?></div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                        </td>
                        <td class="help medium">
                            <?php
                            echo __('At least :num characters.', array(
                                        ':num' => SocialLoginSettings::get('min_username_length'),
                            ));
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="social-label">
                            <label for="register_email">
                                <?php echo __('E-mail'); ?>
                            </label>
                        </td>
                        <td class="field">
                            <input type="email" 
                            <?php echo (!empty($formErrors['email'])) ? 'class="error"' : ''; ?> 
                                   id="register_email" 
                                   placeholder="<?php echo __('your@email.com'); ?>" 
                                   name="register[email]" 
                                   validate="true" 
                                   value="<?php echo (!empty($formValues['email']) ? $formValues['email'] : ''); ?>" />
                                   <?php if ( !empty($formErrors['email']) ): ?>
                                <div class="form-item-errors">
                                    <?php foreach ( $formErrors['email'] as $error ): ?>
                                        <div><?php echo $error; ?></div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                        </td>
                        <td class="help medium">
                            <?php echo __('Must be valid.'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="social-label">
                            <label for="register_password">
                                <?php echo __('Password'); ?>
                            </label>
                        </td>
                        <td class="field">
                            <input type="password" 
                            <?php echo (!empty($formErrors['password'])) ? 'class="error"' : ''; ?> 
                                   id="register_password" 
                                   placeholder="<?php echo __('your password'); ?>"
                                   name="register[password]" 
                                   pattern=".{<?php echo SocialLoginSettings::get('min_password_length'); ?>,}"
                                   validate="true" 
                                   title="<?php echo __('At least :num characters.', array( ':num' => SocialLoginSettings::get('min_password_length') )); ?>"
                                   value="" />
                                   <?php if ( !empty($formErrors['password']) ): ?>
                                <div class="form-item-errors">
                                    <?php foreach ( $formErrors['password'] as $error ): ?>
                                        <div><?php echo $error; ?></div>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                        </td>
                        <td class="help medium">
                            <?php
                            echo __('At least :num characters.', array(
                                        ':num' => SocialLoginSettings::get('min_password_length'),
                            ));
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="social-label">
                            <label for="register_confirm">
                                <?php echo __('Confirm password'); ?>
                            </label>
                        </td>
                        <td class="field">
                            <input type="confirm" 
                            <?php echo (!empty($formErrors['password'])) ? 'class="error"' : ''; ?> 
                                   placeholder="<?php echo __('same as above'); ?>"
                                   validate="true" 
                                   id="register_confirm" 
                                   name="register[confirm]" 
                                   value="" />
                        </td>
                        <td class="help medium">
                            <?php echo __('Type once again.'); ?>
                        </td>
                    </tr>
                    <?php
                    /*
                     * CAPTCHA
                     */
                    ?>
                    <tr>
                        <td class="social-label">
                        </td>
                        <td class="field">
                            <a href="#" onclick="document.getElementById('social-login-captcha').src='<?php echo URL_PUBLIC . SOCIAL_CAPTCHA_URI; ?>?' + Math.random();return false;">
                                <img id="social-login-captcha" src="<?php echo URL_PUBLIC . SOCIAL_CAPTCHA_URI; ?>" alt="<?php echo __('Security check'); ?>" title="<?php echo __('Unreadable? Click the image to refresh.'); ?>"/>
                            </a>
                        </td>
                        <td class="help medium">
                            <?php echo __('Unreadable? Click the image to refresh.'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="social-label">
                            <label for="<?php echo SOCIAL_CAPTCHA_KEY; ?>">
                                <?php echo __('Security check'); ?>
                            </label>
                        </td>
                        <td class="field">
                            <input type="text" 
                            <?php echo (!empty($formErrors['captcha'])) ? 'class="error"' : ''; ?>
                                   id="<?php echo SOCIAL_CAPTCHA_KEY; ?>" 
                                   validate="true" 
                                   name="<?php echo SOCIAL_CAPTCHA_KEY; ?>" 
                                   value="" />
                        </td>
                        <td class="help medium">
                            <?php echo __('Type the word from the image.'); ?>
                        </td>
                    </tr>
                    <?php
                    /*
                     * END OF CAPTCHA
                     */
                    ?>
                </table>
                <div class="actions">
                    <button class="button" name="commit" type="submit" ><?php echo __('Create new account'); ?></button>
                </div>
            </form>
        </div>
    </div>
</div>