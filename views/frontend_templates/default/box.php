<?php
/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage members
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') ) {
    exit();
}


/**
 * Default styles
 */
if ( $settings['default_css'] ) {
    $csspath = PLUGINS_ROOT . '/social_login/views/frontend_templates/' . $settings['template'] . '/box.css';
    if ( file_exists($csspath) ) {
        $cssfile = file_get_contents($csspath);
        print ($cssfile) ? '<style>' . PHP_EOL . $cssfile . '</style>' . PHP_EOL : '';
    }
}
?>
<div id="social_login_box">
    <?php if ( $user ): ?>
        <?php if ( !$settings['hide_wrapper'] ): ?>
            <div class="box-wrapper">
                <div class="box-title">
                    <div>
                        <?php echo __('Hello'); ?> <span class="name"><?php echo $user->name; ?></span>
                    </div>
                </div>
                <div class="box-content">
                <?php endif; ?>
                <?php if ( $settings['show_avatar'] ): ?>
                    <img class="avatar" src="http://www.gravatar.com/avatar.php?gravatar_id=<?php echo md5($user->email); ?>&amp;size=<?php echo (int) $settings['avatar_size']; ?>" alt="avatar" />
                <?php endif; ?>
                <?php if ( $settings['show_username'] ): ?>
                    <div class="username">
                        <?php echo $user->username; ?>
                    </div>
                <?php endif; ?>
                <?php if ( $settings['show_actions'] ): ?>
                    <div class='actions'>
                        <div><a href="<?php echo URL_PUBLIC . SOCIAL_LOGOUT_URI; ?>"><?php echo __('Logout') ?></a></div>
                        <?php if ( AuthUser::hasPermission('admin_view') ): ?>
                            <div><a href="<?php echo URL_PUBLIC . ADMIN_DIR; ?>"><?php echo __('Go to backend'); ?></a></div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <?php if ( !$settings['hide_wrapper'] ): ?>
                </div>
            </div>
        <?php endif; ?>
    <?php else: ?>
        <?php if ( $valid_services || SocialLoginSettings::get('email_enabled') ): ?>
            <?php if ( !$settings['hide_wrapper'] ): ?>
                <div class="box-wrapper">
                    <div class="box-title">
                        <?php echo __('Sign up'); ?>
                    </div>
                    <div class="box-content">
                        <div class="social-buttons">
                        <?php endif; ?>
                        <?php if ( $valid_services ): ?>
                            <?php foreach ( $valid_services as $service ): ?>
                                <a class="social-icon" <?php echo $settings['a_tag']; ?> rel="nofollow" href="<?php echo URL_PUBLIC . SOCIAL_FRONTEND_URI . '/' . $service; ?>">
                                    <img src="<?php echo SOCIAL_ICONS_URI . $service; ?>.png" alt="<?php echo $service . ' ' . __('login'); ?>"/>
                                    <?php if ( $settings['show_labels'] ): ?>
                                        <span><?php echo ucfirst($service); ?></span>
                                    <?php endif; ?>
                                </a>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <?php if ( SocialLoginSettings::get('email_enabled') ): ?>
                            <div><?php echo __('or'); ?></div>
                            <a class="social-icon" <?php echo $settings['a_tag']; ?> rel="nofollow" href="<?php echo URL_PUBLIC . SOCIAL_FRONTEND_URI . '/email'; ?>">
                                <img src="<?php echo SOCIAL_ICONS_URI; ?>email.png" alt="<?php echo __('Register by e-mail'); ?>"/>
                                <?php if ( $settings['show_labels'] ): ?>
                                    <span><?php echo __('Register by e-mail'); ?></span>
                                <?php endif; ?>
                            </a>
                        <?php endif; ?>
                        <?php if ( !$settings['hide_wrapper'] ): ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        <?php else: ?>
            <?php
            if ( SOCIAL_DEBUG )
                echo "<b>DEBUG ON: No valid social services found! Set up your OAuth accounts in Members Plugin settings</b>";
            ?>
        <?php endif; ?>
    <?php endif; ?>
    <?php
    if ( $settings['show_alerts'] ) {
        /**
         * Alert boxes
         */
        $view = new View(SOCIAL_VIEW_FOLDER . 'frontend_templates/' . $settings['template'] . '/alerts');
        echo $view;
    }
    ?>

</div>