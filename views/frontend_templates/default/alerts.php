<div class="alerts">
    <?php
    $flash_errors    = Flash::get(SOCIAL_FLASH_ERROR);
    $flash_successes = Flash::get(SOCIAL_FLASH_SUCCESS);
    $flash_infos     = Flash::get(SOCIAL_FLASH_INFO);
    ?>

    <?php if ( is_array($flash_infos) ): ?>
        <?php foreach ( $flash_infos as $info ): ?>
            <div data-alert class="alert-box info">
                <?php echo $info; ?>
                <a href="#" class="close" onclick="javascript:this.parentNode.parentNode.removeChild(this.parentNode);">&times;</a>
            </div>
        <?php endforeach; ?>
    <?php elseif (!empty($flash_infos)): ?>
        <div data-alert class="alert-box info">
            <?php echo $flash_infos; ?>
            <a href="#" class="close" onclick="javascript:this.parentNode.parentNode.removeChild(this.parentNode);">&times;</a>
        </div>
    <?php endif; ?>

    
    
    
    
    <?php if ( is_array($flash_successes) ): ?>
        <?php foreach ( $flash_successes as $success ): ?>
            <div data-alert class="alert-box success">
                <?php echo $success; ?>
                <a href="#" class="close" onclick="javascript:this.parentNode.parentNode.removeChild(this.parentNode);">&times;</a>
            </div>
        <?php endforeach; ?>
    <?php elseif (!empty($flash_successes)): ?>
        <div data-alert class="alert-box success">
            <?php echo $flash_successes; ?>
            <a href="#" class="close" onclick="javascript:this.parentNode.parentNode.removeChild(this.parentNode);">&times;</a>
        </div>
    <?php endif; ?>

    
    
    
    
    <?php if ( is_array($flash_errors) ): ?>
        <?php foreach ( $flash_errors as $error ): ?>
            <div data-alert class="alert-box error">
                <?php echo $error; ?>
                <a href="#" class="close" onclick="javascript:this.parentNode.parentNode.removeChild(this.parentNode);">&times;</a>
            </div>
        <?php endforeach; ?>
    <?php elseif (!empty($flash_errors)): ?>
        <div data-alert class="alert-box error">
            <?php echo $flash_errors; ?>
            <a href="#" class="close" onclick="javascript:this.parentNode.parentNode.removeChild(this.parentNode);">&times;</a>
        </div>
    <?php endif; ?>
</div>