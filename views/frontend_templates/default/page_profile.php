<?php
/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') ) {
    exit();
}

/**
 * Default styles
 */
if ( $settings['default_css'] ) {
    $csspath = PLUGINS_ROOT . '/social_login/views/frontend_templates/' . $settings['template'] . '/page.css';
    if ( file_exists($csspath) ) {
        $cssfile = file_get_contents($csspath);
        print ($cssfile) ? '<style>' . PHP_EOL . $cssfile . '</style>' . PHP_EOL  : '';
    }
}
?>



<div id="social_login_page">
    <?php
    /**
     * Alert boxes
     */
    if ( (bool) $settings['show_alerts'] ) {
        $view = new View(SOCIAL_VIEW_FOLDER . 'frontend_templates/' . $settings['template'] . '/alerts');
        echo $view;
    }
    ?>

    <?php if ( $settings['show_welcome_header'] ): ?>
        <div class="profile-welcome-header">
            <?php echo __('Hello'); ?> <span class="name"><?php echo $user->name; ?></span>
        </div>
    <?php endif; // $settings['show_welcome_header']  ?>

    <?php
    if ( $settings['show_account_info'] ) {
        if ( $user ) {
            $view = new View(SOCIAL_VIEW_FOLDER . 'frontend_templates/' . $settings['template'] . '/page_profile_account_info', array(
                        'user'             => $user,
                        'profile_page_uri' => $profile_page_uri,
                        'settings'         => $settings,
            ));
            echo $view;
        }
    }

    if ( $settings['show_account_manage'] ) {
        if ( $user && AuthUser::hasPermission('social_edit_account') ) {
            $view = new View(SOCIAL_VIEW_FOLDER . 'frontend_templates/' . $settings['template'] . '/page_profile_account_manage', array(
                        'user'             => $user,
                        'profile_page_uri' => $profile_page_uri,
                        'formValues'       => $formValues,
                        'settings'         => $settings,
            ));
            echo $view;
        }
    }

    if ( $settings['show_social_identities'] ) {
        $view = new View(SOCIAL_VIEW_FOLDER . 'frontend_templates/' . $settings['template'] . '/page_profile_social_identities', array(
                    'social'           => $social,
                    'profile_page_uri' => $profile_page_uri,
                    'settings'         => $settings,
        ));
        echo $view;
    }
    ?>

</div>