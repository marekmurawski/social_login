<?php

/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') ) {
    exit();
}


class SocialLoginController extends PluginController {

    const VIEW_FOLDER = '../../plugin/social_login/views/';

    public function __construct() {
        $this->setLayout('backend');
        $this->assignToLayout('sidebar', new View('../../plugins/social_login/views/sidebar'));

    }


    public function index() {
        $this->identities();

    }


    public function identities() {
        if ( !AuthUser::hasPermission('user_edit') )
            redirect('/');
        $whereString    = '';
        $service_filter = '';
        $userid_filter  = '';

        if ( isset($_GET['service_filter']) && (strlen($_GET['service_filter']) > 0) ) {
            $service_filter = $_GET['service_filter'];
            $whereString .= ' service_type=' . Record::escape($_GET['service_filter']);
        }
        else
            $whereString = '1=1';


        if ( isset($_GET['userid_filter']) && (strlen($_GET['userid_filter']) > 0) ) {
            $whereString .= ' AND user_id=' . (int) $_GET['userid_filter'];
            $userid_filter = (int) $_GET['userid_filter'];
        };

        $socialAccounts = UserSocial::find(array(
                                'order'     => 'user_id ASC',
                                'where'     => $whereString,
                                'with_user' => 1,
                                //'debug'     => 1
                                )
        );
        $this->display('social_login/views/backend/identities', array(
                    'socialAccounts' => $socialAccounts,
                    'service_filter' => $service_filter,
                    'userid_filter'  => $userid_filter,
                    'users'          => Record::findAllFrom('User', ' 1=1 ORDER BY username'),
        ));

    }


    public function pending() {
        if ( !AuthUser::hasPermission('user_edit') )
            redirect('/');

        $this->display('social_login/views/backend/pending', array(
                    'userPending' => UserPending::findAll()
        ));

    }


    public function documentation() {
        $this->display('social_login/views/documentation');

    }


    public function oauth_status() {
        // if ( SOCIAL_DEBUG ) {
        //echo '<pre>' . print_r(Flash::, true) . '</pre>';
        echo '<pre>' . print_r($_SESSION, true) . '</pre>';
        die();
        // } else {
        //     echo "Debug mode off!";
        //     die();
        // }

    }


    /**
     * Convert a hexa decimal color code to its RGB equivalent
     *
     * @param string $hexStr (hexadecimal color value)
     * @param boolean $returnAsString (if set true, returns the value separated by the separator character. Otherwise returns associative array)
     * @param string $seperator (to separate RGB values. Applicable only if second parameter is true.)
     * @return array or string (depending on second parameter. Returns False if invalid hex color value)
     */
    private function hex2RGB($hexStr, $returnAsString = false, $seperator = ',') {
        $hexStr   = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // Gets a proper hex string
        $rgbArray = array( );
        if ( strlen($hexStr) == 6 ) { //If a proper hex code, convert using bitwise operation. No overhead... faster
            $colorVal          = hexdec($hexStr);
            $rgbArray['red']   = 0xFF & ($colorVal >> 0x10);
            $rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
            $rgbArray['blue']  = 0xFF & $colorVal;
        } elseif ( strlen($hexStr) == 3 ) { //if shorthand notation, need some string manipulations
            $rgbArray['red']   = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
            $rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
            $rgbArray['blue']  = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
        } else {
            return false; //Invalid hex color code
        }
        return $returnAsString ? implode($seperator, $rgbArray) : $rgbArray; // returns the rgb string or the associative array

    }


    public function captcha_image($width = false, $height = false) {

        $captcha                  = new SimpleCaptcha();
        $captcha->resourcesPath   = PLUGINS_ROOT . DS . 'social_login' . DS . 'lib' . DS . 'cool-php-captcha-0.3.1' . DS . 'resources';
        //$captcha->width           = ($width && ((int) $width > 50)) ? (int) $width : 200;
        //$captcha->height          = ($height && ((int) $height > 30)) ? (int) $height : 80;
        $captcha->wordsFile       = 'words/en_positive.php'; // Dictionary word file (empty for random text)
        //$captcha->minWordLength   = 5; // Min word length (for non-dictionary random text generation)
        //$captcha->maxWordLength   = 8;
        $captcha->session_var     = SOCIAL_CAPTCHA_KEY;
        $captcha->backgroundColor = array( 240, 240, 240 );
        $captcha->colors          = array(
                    array( 27, 78, 181 ), // blue
                    array( 22, 163, 35 ), // green
                    array( 214, 36, 7 ), // red
        );
        $captcha->shadowColor     = array( 128, 128, 128 ); // array(0, 0, 0);
        $captcha->lineWidth       = 2; // Horizontal line through the text
        /**
         * Font configuration
         * - font: TTF file
         * - spacing: relative pixel space between character
         * - minSize: min font size
         * - maxSize: max font size
         */
        $captcha->fonts           = array(
                    'Duality'  => array( 'spacing' => -2, 'minSize' => 30, 'maxSize' => 38, 'font'    => 'Duality.ttf' ),
                    'Heineken' => array( 'spacing' => -2, 'minSize' => 24, 'maxSize' => 34, 'font'    => 'Heineken.ttf' ),
                    'Jura'     => array( 'spacing' => -2, 'minSize' => 28, 'maxSize' => 32, 'font'    => 'Jura.ttf' ),
                    'StayPuft' => array( 'spacing' => -1.5, 'minSize' => 28, 'maxSize' => 32, 'font'    => 'StayPuft.ttf' ),
        );
        // Wave configuration in X and Y axes
        $captcha->Yperiod         = 14;
        $captcha->Yamplitude      = 8;
        $captcha->Xperiod         = 13;
        $captcha->Xamplitude      = 5;
        $captcha->maxRotation     = 8; // letter rotation clockwise
        $captcha->scale           = 2; // 1: low, 2: medium, 3: high
        $captcha->blur            = false;
        $captcha->debug           = false; //** Debug?
        $captcha->imageFormat     = 'png'; // Image format: jpeg or png
        $captcha->CreateImage();
        exit();

    }


    /**
     * GOOGLE: https://accounts.google.com/o/oauth2/revoke?token={token}
     */
    public function oauth_call_api($service = false) {

        $userSocial = new UserSocial($service);

        $token = $userSocial->getOAuthToken();

        $request = file_get_contents('https://accounts.google.com/o/oauth2/revoke?token=' . $token);
        echo $request;
        $userSocial->callApi('https://accounts.google.com/o/oauth2/revoke', 'GET', array( 'token' => $token ));
        $oauth   = $userSocial->getOAuth();
        $oauth->Output();
        echo '<pre>' . print_r($oauth, true) . '</pre>';
        die();

    }


    public function oauth_logout($service = false) {
        if ( !$service ) {
            Observer::notify('log_event', __('User <b>:name</b> logged out.', array(
                        ':name' => AuthUser::getUserName(),
                                    ), 'social_login'));
            AuthUser::logout();
            Flash::set(SOCIAL_FLASH_SUCCESS, __('You have logged out!'));
            if ( isset($_SESSION['OAUTH_ACCESS_TOKEN']) )
                unset($_SESSION['OAUTH_ACCESS_TOKEN']);
            $this->redir(Social::getLastUrl());
        }

    }


    private function redir($url) {
        Social::setLoginRequiredUrl(null, true);
        Social::setLastUrl(null, true);
        if ( SOCIAL_DEBUG ) {
            echo 'DEBUG: Redirecting to: ' . $url . '<br/>' . PHP_EOL;
            echo 'Continue to: <a href="' . $url . '">' . $url . '</a>';
            die();
        } else {
            header('HTTP/1.1 302 Found', true);
            header('Location: ' . $url);
            die();
        }

    }


    public function find_email_register_page() {
        /**
         * EMAIL REGISTER
         */
        if ( SocialLoginSettings::get('email_enabled') ) {
            $page = Page::find(array(
                                    'where' => 'behavior_id=' . Record::escape('social_profile'),
                                    'limit' => 1,
                                    )
            );

            if ( !$page ) {
                echo 'Profile page not found!<br/>';
                if ( SOCIAL_DEBUG ) {
                    echo 'DEBUG: You need to set page behavior (page type) to "Profile" in order to Social Login work properly';
                }
                return false;
            } else {
                if ( $page->partExists('settings') ) {
                    $pageSettings = mmCore::parseValues($page->part->settings->content_html, '=>', '#');
                }
                $slugRegister = isset($pageSettings['slug_register']) ? $pageSettings['slug_register'] : 'register';

                $registerUrl = $page->url(false) . '/' . $slugRegister;
                header('HTTP/1.1 302 Found', true);
                header('Location: ' . $registerUrl);
                die();
            }
        } else {
            die("Email registration disabled!");
        }

    }


    public function oauth_login($service = false) {
        /**
         * NOW OAUTH PROCESSING
         */
        $validServices = SocialLoginSettings::getValidServices();
        if ( !$validServices ) {
            die("No authentication services configured!");
        }

        if ( is_array($validServices) && !in_array($service, $validServices) ) {
            die("Authentication service not found! - " . $service);
        }

        if ( isset($_GET['action']) ) {
            if ( $_GET['action'] === 'connect' ) {
                $action = 'connect';
                Flash::set('social_login_connect_in_progress', $service);
            } elseif ( $_GET['action'] === 'disconnect' ) {
                $action    = 'disconnect';
                $social_id = $_GET['id'];
            } else {
                die('invalid request!');
            }
        } else {
            $action = 'login';
        }

        $progress = Flash::get('social_login_connect_in_progress');
        if ( $progress !== null )
            $action   = 'connect';

        $socialUser = new UserSocial($service);
        if ( $action === 'login' ) {
            if ( $socialUser->try_login() ) {
                Flash::set(SOCIAL_FLASH_SUCCESS, Flash::get('usersocial_success'));
            } else {
                if ( SOCIAL_DEBUG )
                    echo Flash::get('usersocial_error');
                Flash::set(SOCIAL_FLASH_ERROR, Flash::get('usersocial_error'));
            }
        } elseif ( $action === 'connect' ) {
            if ( AuthUser::isLoggedIn() ) {
                echo "User is loggedin - " . AuthUser::getUserName() . '<br/>';
                if ( $socialUser->connect() ) {
                    if ( SOCIAL_DEBUG )
                        echo 'Connection successful - ' . $service . '<br/>' . PHP_EOL;
                    Flash::set(SOCIAL_FLASH_SUCCESS, Flash::get('usersocial_success'));
                } else {
                    if ( SOCIAL_DEBUG )
                        echo Flash::get('usersocial_error');
                    Flash::set(SOCIAL_FLASH_ERROR, Flash::get('usersocial_error'));
                }
            } else {
                Flash::set(SOCIAL_FLASH_ERROR, 'You are not logged in!');
                $this->redir(URL_PUBLIC);
            }
        } elseif ( $action === 'disconnect' ) {
            if ( AuthUser::isLoggedIn() ) {
                /**
                 * Check if user is trying to disconnect the last service
                 * if his password is empty and he tries to disconnect it
                 * prevent the action
                 */
                $userIdentitiesCount = Record::countFrom('UserSocial', 'user_id=:id', array( ':id' => AuthUser::getId() ));
                if ( $userIdentitiesCount < 2 ) {
                    if ( strlen(AuthUser::getRecord()->password) < 1 ) {
                        Flash::set(SOCIAL_FLASH_ERROR, 'You cannot disconnect the last identity when your <b>password is empty</b>!');
                        $this->redir(Social::getLastUrl());
                    }
                }

                echo "User is loggedin - " . AuthUser::getUserName() . '<br/>';
                $disconnectUserSocial = UserSocial::findByService($service, $social_id);
                if ( $disconnectUserSocial->disconnect() ) {
                    Flash::set(SOCIAL_FLASH_SUCCESS, Flash::get('usersocial_success'));
                } else {
                    Flash::set(SOCIAL_FLASH_ERROR, Flash::get('usersocial_error'));
                    echo Flash::get('usersocial_error');
                }
            } else {
                Flash::set(SOCIAL_FLASH_ERROR, 'You are not logged in!');
                $this->redir(URL_PUBLIC);
            }
        }
        // redirecting back

        if ( strlen(Social::getLoginRequiredUrl()) > 0 ) {
            $url = Social::getLoginRequiredUrl();
            Social::setLoginRequiredUrl(null, true);
        } else {
            $url = Social::getLastUrl();
            Social::setLastUrl(null, true);
        }
        $this->redir($url);

    }


    public function confirm_account($reg_hash) {
        $userPending = Record::findOneFrom('UserPending', 'reg_hash=:reg_hash', array( ':reg_hash' => $reg_hash ));
        if ( $userPending ) {
            echo '<pre>' . print_r($userPending, true) . '</pre>';
            $newUser           = new User();
            $newUser->name     = $userPending->name;
            $newUser->username = $userPending->username;
            $newUser->email    = $userPending->email;
            $newUser->password = $userPending->password;
            $newUser->salt     = $userPending->reg_salt;
            if ( $newUser->save() ) {


                if ( SOCIAL_DEBUG )
                    echo 'DEBUG: Created new Wolf User account - ' . $newUser->username . '<br/>' . PHP_EOL;

                $newUserRolesStr = trim(SocialLoginSettings::get('newuser_roles'), ', ');
                $newUserRolesArr = explode(',', $newUserRolesStr);
                foreach ( $newUserRolesArr as $newRoleStr ) {
                    // cleanup
                    $newRoleStr = trim($newRoleStr);
                    if ( $role       = Role::findByName($newRoleStr) ) {
                        if ( version_compare(CMS_VERSION, '0.7.5', '<=') ) {
                            UserRole::setPermissionsFor($newUser->id, array( $role->id )); // WOLF 0.7.5 uses this
                            if ( SOCIAL_DEBUG )
                                echo 'DEBUG: Role <b>' . $newRoleStr . '</b> added for user <b>' . $newUser->name . '</b><br/>';
                        } else {
                            UserRole::setRolesFor($newUser->id, array( $role->id )); // WOLF 0.7.6 and later
                            if ( SOCIAL_DEBUG )
                                echo 'DEBUG: Role <b>' . $newRoleStr . '</b> added for user <b>' . $newUser->name . '</b><br/>';
                        }
                    }
                }

                // force login
                AuthUser::forceLogin($newUser->username);

                Flash::set(SOCIAL_FLASH_SUCCESS, __('You have logged in!'));
                Flash::set('success', __('You have logged in!'));
            } else {
                if ( SOCIAL_DEBUG )
                    echo 'DEBUG: DB Error creating Wolf User account - ' . $newUser->username . '<br/>' . PHP_EOL;
                Flash::set(SOCIAL_FLASH_ERROR, __('Error creating your account!'));
                Flash::set('error', __('Error creating your account!'));
            }
            // redirecting
            $this->redir(URL_PUBLIC);
        } else {
            echo ('Registration entry not found!');
            echo '<br/>';
            echo '<a href="' . URL_PUBLIC . '">' . __('Go to homepage') . '</a>';
        }

    }


    public function identity_delete() {
        if ( !isset($_POST['id']) || get_request_method() != 'AJAX' )
            mmCore::failure('Invalid POST data or NON-AJAX call!');
        $identity_id = (int) $_POST['id'];
        $userSocial  = Record::findByIdFrom('UserSocial', $identity_id);
        if ( $userSocial->delete() ) {
            mmCore::success(__('Deleted identity <b>:id</b>', array( ':id' => $userSocial->social_id )));
        } else {
            mmCore::failure(__('Error deleting identity of <b>:id</b>', array( ':id' => $userSocial->social_id )));
        }

    }


    public function pending_delete() {
        if ( !isset($_POST['id']) || get_request_method() != 'AJAX' )
            mmCore::failure('Invalid POST data or NON-AJAX call!');
        $pending_id  = (int) $_POST['id'];
        $userPending = Record::findByIdFrom('UserPending', $pending_id);
        if ( $userPending->delete() ) {
            mmCore::success(__('Deleted pending registration <b>:name</b>', array( ':name' => $userPending->name )));
        } else {
            mmCore::failure(__('Error deleting pending registration of <b>:name</b>', array( ':name' => $userPending->name )));
        }

    }


    /**
     * Shows details of user connected identities
     * under Wolf Users Details
     *
     * @param type $user
     */
    public static function Callback_user_edit_view_after_details($user) {
        $socialAccounts = UserSocial::findByWolfUserId($user->id);
        $view           = new View(SOCIAL_VIEW_FOLDER . 'backend/user_edit_view_after_details', array(
                    'socialAccounts' => $socialAccounts,
        ));
        echo $view;

    }


    /**
     * Works only for Wolf CMS 0.7.6 and later
     *
     *
     * @param type $username
     * @param type $user_id
     */
    public static function Callback_user_after_delete($username, $user_id) {
        if ( isset($user_id) ) {
            Record::deleteWhere('UserSocial', 'user_id=:uid', array( ':uid' => $user_id ));
        }

    }


    public static function Callback_login_required($redirect) {
        // handle only if we got redirected
        if ( !empty($redirect) ) {

            $page = Page::find(array(
                                    'where' => 'behavior_id=' . Record::escape('social_profile'),
                                    'limit' => 1,
                                    )
            );

            if ( !$page ) {
                echo 'Social Login Plugin: Page with behavior "social_profile" not found!';
                return false;
            } else {

                /*
                 *  SPECIAL CASE - user requested password recovery
                 *  in backend interface
                 *  if so, RETURN and do nothing
                 */
                if ( strpos($redirect, ADMIN_DIR . '/login/forgot') !== false ) {
                    return false;
                }
                $loginUrl = URL_PUBLIC . $page->getUri();
                Social::setLoginRequiredUrl($redirect);
                Flash::set('login_required', $redirect); // used for displaying notice in frontend
                // self::redir($loginUrl);
                header('HTTP/1.1 302 Found', true);
                header('Location: ' . $loginUrl);
                die();
            }
        }

    }


    public function settings() {
        $this->display('social_login/views/settings', array(
                    'settings' => Plugin::getAllSettings('social_login')
        ));

    }


    public function settings_store() {
        if ( !empty($_POST['settings']) ) {
            if ( Plugin::setAllSettings($_POST['settings'], 'social_login') ) {
                Flash::set('success', __('Settings saved!'));
            } else {
                Flash::set('error', __('Error saving settings!'));
            }
        }
        else
            Flash::set('error', __('No POST data!'));

        redirect(get_url('plugin/social_login/settings'));

    }


}