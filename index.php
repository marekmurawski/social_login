<?php

/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') ) {
    exit();
}

Plugin::setInfos(array(
            'id'                   => 'social_login',
            'title'                => __('Social Login'),
            'description'          => __('Provides OAuth social login and account management. (Google, Facebook, Twitter)'),
            'version'              => '0.0.9-dev',
            'license'              => 'GPL',
            'author'               => 'Marek Murawski',
            'website'              => 'http://marekmurawski.pl/',
            'require_wolf_version' => '0.7.6',
            'type'                 => 'both'
));

// todo remove associated identities after user delete
// Observer::observe('user_after_delete', $user->name);

            const SOCIAL_VIEW_FOLDER = '../../plugins/social_login/views/';

if ( Plugin::isEnabled('social_login') ) {
    AutoLoader::addFile(array(
                'UserSocial'          => dirname(__FILE__) . '/models/UserSocial.php',
                'UserPending'         => dirname(__FILE__) . '/models/UserPending.php',
                'SocialLoginSettings' => dirname(__FILE__) . '/SocialLoginSettings.php',
                'oauth_client_class'  => dirname(__FILE__) . '/lib/oauth/oauth_client.php',
                'http_class'          => dirname(__FILE__) . '/lib/oauth/http.php',
                'SimpleCaptcha'       => dirname(__FILE__) . '/lib/cool-php-captcha-0.3.1/captcha.php',
    ));

    Observer::observe('user_edit_view_after_details', 'SocialLoginController::Callback_user_edit_view_after_details');
    Observer::observe('user_after_delete', 'SocialLoginController::Callback_user_after_delete');
    Observer::observe('login_required', 'SocialLoginController::Callback_login_required');
    Observer::observe('login_requested', 'SocialLoginController::Callback_login_requested');
    Plugin::addController('social_login', __('Social login'), 'user_edit', true);

    // conditional add Javascript helpers
    $uri = $_SERVER['QUERY_STRING'];
    if ( preg_match('#\/plugin\/social_login#', $uri) ) {
        Plugin::addJavascript('social_login', 'js/jquery.tablesorter.js');
        Plugin::addJavascript('social_login', 'js/jquery.tablesorter.pager.js');
    }

    // don't override CMS_ROOT/config.php setting
    if ( !defined('SOCIAL_DEBUG') ) {
        $social_login_debug_mode = SocialLoginSettings::get('debug_mode');
        define('SOCIAL_DEBUG', $social_login_debug_mode);
    }
    define('SOCIAL_FRONTEND_URI', 'oauth2');
    define('SOCIAL_ICONS_URI', PLUGINS_URI . 'social_login/icons/services/'); // must end with slash
    define('SOCIAL_LOGOUT_URI', SOCIAL_FRONTEND_URI . '_logout');
    define('SOCIAL_CONFIRM_EMAIL_URI', 'confirm-account');

    define('SOCIAL_FLASH_SUCCESS', 'social_login_flash_success');
    define('SOCIAL_FLASH_ERROR', 'social_login_flash_error');
    define('SOCIAL_FLASH_FORM_ERROR', 'social_login_flash_form_error');
    define('SOCIAL_FLASH_INFO', 'social_login_flash_info');

    define('SOCIAL_CAPTCHA_URI', 'social-captcha.png');
    define('SOCIAL_CAPTCHA_KEY', 'social_captcha');

    Behavior::add('social_profile', 'social_login/SocialProfile.php');

    Dispatcher::addRoute('/' . SOCIAL_FRONTEND_URI . '/email', '/plugin/social_login/find_email_register_page');
    Dispatcher::addRoute('/' . SOCIAL_FRONTEND_URI . '/:any', '/plugin/social_login/oauth_login/$1');
    Dispatcher::addRoute('/' . SOCIAL_FRONTEND_URI . '_status', '/plugin/social_login/oauth_status');
    Dispatcher::addRoute('/' . SOCIAL_LOGOUT_URI, '/plugin/social_login/oauth_logout');
    Dispatcher::addRoute('/' . SOCIAL_LOGOUT_URI . '/:any', '/plugin/social_login/oauth_logout/$1');
    Dispatcher::addRoute('/' . SOCIAL_CONFIRM_EMAIL_URI . '/:any', '/plugin/social_login/confirm_account/$1');
    Dispatcher::addRoute('/' . SOCIAL_CAPTCHA_URI, '/plugin/social_login/captcha_image');
}


class Social {

    public static function getLastUrl() {
        return (isset($_SESSION['SOCIAL_LAST_URL'])) ? $_SESSION['SOCIAL_LAST_URL'] : '';

    }


    public static function setLastUrl($url = null, $reset = false) {
        if ( $reset ) {
            // if ( SOCIAL_DEBUG )
            // echo 'Reset LAST_URL Called!<br/>';
            unset($_SESSION['SOCIAL_LAST_URL']);
            return true;
        }
        if ( empty($url) ) {
            $_SESSION['SOCIAL_LAST_URL'] = URL_PUBLIC . CURRENT_URI;
        } else {
            $_SESSION['SOCIAL_LAST_URL'] = $url;
        }

    }


    public static function getLoginRequiredUrl() {
        return (isset($_SESSION['SOCIAL_LOGIN_REQUIRED_URL'])) ? $_SESSION['SOCIAL_LOGIN_REQUIRED_URL'] : '';

    }


    public static function setLoginRequiredUrl($url = null, $reset = false) {
        if ( $reset ) {
            // if ( SOCIAL_DEBUG )
            // echo 'Reset LOGIN_REQUIRED_URL Called!';
            unset($_SESSION['SOCIAL_LOGIN_REQUIRED_URL']);
            return true;
        }
        if ( empty($url) ) {
            $_SESSION['SOCIAL_LOGIN_REQUIRED_URL'] = URL_PUBLIC . CURRENT_URI;
        } else {
            $_SESSION['SOCIAL_LOGIN_REQUIRED_URL'] = $url;
        }

    }


    /**
     * Renders Social login box / infobox
     * 
     * <code>
     *  
     *  if (Plugin::isEnabled('social_login')) {
     *      Social::loginBox(array(
     *              'template'       => 'default',
     *              'language'       => 'en',
     *              'default_css'    => 1,
     *              'show_username'  => 1,
     *              'show_logout'    => 1,
     *              'show_actions'   => 1,
     *              'show_connected' => 1,
     *              'show_avatar'    => 1,
     *              'show_labels'    => 1,
     *              'a_tag'          => style="display: inline-block",
     *              'avatar_size'    => 32,
     *              'show_alerts'    => 1,
     *      ));
     *  }
     *  
     * </code>
     * 
     * @param Array $settings
     */
    public static function loginBox($boxSettings = array( )) {

        $defaultSettings = array(
                    'template'       => 'default',
                    'language'       => 'en',
                    'default_css'    => 1,
                    'hide_wrapper'   => 1,
                    'show_username'  => 1,
                    'show_logout'    => 1,
                    'show_actions'   => 1,
                    'show_connected' => 1,
                    'show_avatar'    => 1,
                    'show_labels'    => 1,
                    'a_tag'          => 'style="display: inline-block; background: none; border: none;"',
                    'avatar_size'    => 32,
                    'show_alerts'    => 1,
        );
        $settings        = array_merge($defaultSettings, $boxSettings);

        // store current page url for later use
        self::setLastUrl();
        $social = false;
        if ( $user   = AuthUser::getRecord() ) {
            if ( isset($settings['show_connected']) && (bool) $settings['show_connected'] ) {
                $social = UserSocial::findByWolfUserId($user->id);
            }
        }

        /**
         * load frontent translations
         */
        $old_locale = I18n::getLocale();
        $language   = isset($settings['language']) ? $settings['language'] : 'en';
        I18n::setLocale($language);

        $view = new View(SOCIAL_VIEW_FOLDER . 'frontend_templates/' . $settings['template'] . '/box', array(
                    'user'           => $user,
                    'social'         => $social,
                    'settings'       => $settings,
                    'valid_services' => SocialLoginSettings::getValidServices(),
        ));
        echo $view;
        /**
         * restore I18n locale
         */
        I18n::setLocale($old_locale);

    }


}