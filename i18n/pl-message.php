<?php

/**
 * English file for plugin social_login
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Your Name < email@domain.something >
 * @version Wolf 0.7.7
 */

return array(
    '-no roles assigned-'                      => '-brak przypisanych ról-',
    'Account Settings'                         => 'Ustawienia Konta',
    'Account created on'                       => 'Konto utworzone',
    'Actions'                                  => 'Działania',
    'An email has been sent with your new password!'
 => 'Wysłaliśmy do Ciebie email z nowym hasłem!',
    'Are you sure you want to delete it?'      => 'Czy jesteś pewien, że chcesz to usunąć?',
    'At least :num characters.'                => 'Przynajmniej :num znaków.',
    'Avatar images are associated to your e-mail address via gravatar.com'
 => 'Awatary są przyporządkowane do twojego adresu email przez serwis gravatar.com',
    'Cannot use email: <b>:email</b> Other user already uses this email!'
 => 'Nie można użyć emaila <b>:email</b> Inny użytkownik już używa tego adresu!',
    'Check your mailbox for confirmation email with link to activate your account!'
 => 'Sprawdź swoją pocztę, aby aktywować twoje konto.',
    'Close'                                    => 'Zamknij',
    'Confirm password'                         => 'Potwierdź hasło',
    'Connect :service to your account'         => 'Podłącz :service do Twojego konta',
    'Could not obtain user data from OAuth provider - :type'
 => 'Nie udało się uzyskać danych od dostawcy OAuth - :type',
    'Create new account'                       => 'Utwórz nowe konto',
    'Currently available roles'                => 'Obecnie dostępne role',
    'DB error while authenticating with social identity - :id (:type)'
 => 'Błąd bazy danych podczas uwierzytelniania tożsamości :id (:type)',
    'DB error while creating new account for social identity - :id (:type)'
 => 'Błąd bazy danych podczas tworzenia nowego konta - :id (:type)',
    'Data provided'                            => 'Udostępnione dane',
    'Debug mode'                               => 'Tryb debugowania',
    'Delete identity'                          => 'Usuń tożsamość',
    'Delete pending registration'              => 'Usuń oczekującą rejestrację',
    'Deleted identity <b>:id</b>'              => 'Usunięto tożsamość <b>:id</b>',
    'Deleted pending registration <b>:name</b>'
 => 'Usunięto oczekującą rejestrację <b>:name</b>',
    'Disconnect :service from your account'    => 'Odłącz :service od Twojego konta',
    'Display your documentation here!'         => 'Wyświetl tutaj dokumentację!',
    'Documentation'                            => 'Dokumentacja',
    'E-mail'                                   => 'E-mail',
    'E-mail in service'                        => 'E-mail w usłudze',
    'Email'                                    => 'Email',
    'Email in service'                         => 'E-mail w usłudze',
    'Email registration is off.'               => 'Rejestracja przez email wyłączona.',
    'Error connecting social identity :id (:type) to your account!'
 => 'Błąd podczas podłączania tożsamości :id (:type) do twojego konta!',
    'Error creating new account. Please contact administrator!'
 => 'Błąd przy tworzeniu konta. Skontaktuj się z administratorem!',
    'Error creating your account!'             => 'Błąd przy tworzeniu konta.',
    'Error deleting identity of <b>:id</b>'    => 'Błąd podczas usuwania tożsamości <b>:id</b>',
    'Error deleting pending registration of <b>:name</b>'
 => 'Błąd usuwania oczekującej rejestracji <b>:name</b>',
    'Error generating new password!'           => 'Błąd tworzenia nowego hasła!',
    'Error saving changes!'                    => 'Błąd podczas zapisywania zmian!',
    'Error saving settings!'                   => 'Błąd podczas zapisywania ustawień!',
    'Error while logging you in!'              => 'Błąd podczas logowaania',
    'Facebook'                                 => 'Facebook',
    'Fill the fields below to change password for your account.'
 => 'Uzupełnij poniższe pola, aby zmienić hasło do twojego konta.',
    'Filter by Username'                       => 'Filtruj wg. nazwy użytkownika',
    'Filter by service'                        => 'Filtruj wg. usługi',
    'Forgot password?'                         => 'Zapomniałeś hasła?',
    'General settings'                         => 'Ustawienia ogólne',
    'Github'                                   => 'Github',
    'Go to backend'                            => 'Przejdź do panelu zarządzania',
    'Go to homepage'                           => 'Przejdź do strony głównej',
    'Go to login page'                         => 'Przejdź do strony logowania',
    'Google'                                   => 'Google',
    'Hello'                                    => 'Cześć',
    'Hello, :name'                             => 'Cześć, :name',
    'ID'                                       => 'ID',
    'ID in service'                            => 'ID w usłudze',
    'If you did not submit account registration form, please ignore this email.'
 => 'Jeśli to nie ty podałeś ten adres przy zakładaniu konta, zignoruj tę wiadomość.',
    'If you forgot your password, you can reset it here by providing your e-mail address.'
 => 'Jeśli zapomniałeś swojego hasła, możesz je zrestartować podając poniżej twój adres e-mail.',
    'If you leave your password empty you will only be able to login to this site with social identities attached to your accout.'
 => 'Jeśli zostawisz hasło puste będziesz mógł się zalogować do konta jedynie przy użyciu tożsamości społecznościowych.',
    'Invalid email address!'                   => 'Niepoprawny adres email!',
    'Invalid security check. Try again!'       => 'Niepoprawny kod. Spróbuj ponownie!',
    'JSON data preview'                        => 'podgląd danych JSON',
    'Last failed login'                        => 'Ostatnie nieudane logowanie',
    'Last login'                               => 'Ostatnie logowanie',
    'Login'                                    => 'Zaloguj',
    'Login failed. Please check your login data and try again.'
 => 'Logowanie nieudane. Sprawdź dane i spróbuj ponownie.',
    'Login successful!'                        => 'Logowanie udane!',
    'Logo'                                     => 'Logo',
    'Logout'                                   => 'Wyloguj',
    'Must be valid.'                           => 'Musi być prawidłowy.',
    'Name'                                     => 'Imię i nazwisko',
    'Name in service'                          => 'Nazwa w usłudze',
    'Name must contain at least <b>:num characters</b>!'
 => 'Imię i nazwisko musi zawierać przynajmniej <b>:num znaków</b>!',
    'New account registration in :site_title'  => 'Rejestracja nowego konta w :site_title',
    'New password'                             => 'Nowe hasło',
    'New password confirm'                     => 'Potwierdź nowe hasło',
    'No'                                       => 'Nie',
    'No POST data!'                            => 'Brak danych w POST!',
    'No data sent!'                            => 'Nie wysłano danych!',
    'Note: <b>Your password is empty</b>. It seems your account was created using social login.'
 => 'Uwaga: <b>Twoje hasło jest puste</b>. Wygląda na to, że twoje konto zostało założone przy użyciu logowania społecznościowego.',
    'Note: You can hold SHIFT to delete instanty'
 => 'Podpowiedź: przytrzymaj SHIFT aby usunąć natychmiast',
    'OAuth authorization_error - :type'        => 'Błąd autoryzacji OAuth - :type',
    'OAuth authorization_error - :type - :error'
 => 'Błąd autoryzacji OAuth - :type - :error',
    'Off'                                      => 'Wył.',
    'Offline access'                           => 'Dostęp offline',
    'On'                                       => 'Wł.',
    'Optional. If set, should be valid.'       => 'Opcjonalny. Jeśli ustawiony, powinien być prawidłowy.',
    'Password'                                 => 'Hasło',
    'Password and Confirm were <b>not the same</b> or <b>too short</b>!'
 => 'Hasło i jego powierdzenie <b>nie są takie same</b> lub <b>za krótkie</b>!',
    'Password and confirmation are not the same!'
 => 'Hasło i jego powierdzenie nie są takie same!',
    'Password cannot be the same as the <b>username</b>!'
 => 'Hasło nie może być takie samo jak <b>nazwa użytkownika</b>!',
    'Password cannot be the same as the username!'
 => 'Hasło nie może być takie samo jak nazwa użytkownika!',
    'Password must contain at least <b>:num characters</b>!'
 => 'Hasło musi zawierać przynajmniej <b>:num znaków</b>!',
    'Pending email registrations'              => 'Oczekujące rejestracje email',
    'Please provide password confirm!'         => 'Wpisz potwierdzenie hasła!',
    'Please provide password!'                 => 'Wpisz hasło!',
    'Please provide your email address!'       => 'Wpisz twój email!',
    'Please your name!'                        => 'Podaj twoje imię i nazwisko!',
    'Please your username!'                    => 'Wpisz nazwę użytkownika!',
    'Provides OAuth social login and account management. (Google, Facebook, Twitter)'
 => 'Umożliwia logowanie społecznościowe Google, Facebook i Twitter',
    'Register by e-mail'                       => 'Zarejestruj przez email',
    'Register new account'                     => 'Zarejestruj nowe konto',
    'Register user'                            => 'Zarejestruj użytkownika',
    'Registration date'                        => 'Data rejestracji',
    'Request password reset'                   => 'Zatwierdź reset hasła',
    'Save'                                     => 'Zapisz',
    'Save account settings'                    => 'Zapisz ustawienia Konta',
    'Saved changes!'                           => 'Zmiany zapisane!',
    'Security check'                           => 'Kod bezpieczeństwa',
    'See provided JSON data'                   => 'Zobacz udostępnione dane JSON',
    'Send password'                            => 'Wyślij hasło',
    'Service ID'                               => 'ID usługi',
    'Service name'                             => 'Nazwa usługi',
    'Service type'                             => 'Typ usługi',
    'Set your new password to be able to login using username <b>:username</b> and your new password.'
 => 'Ustaw nowe hasło, aby móc zalogować się używając nazwy użytkownika <b>:username</b> i nowego hasła.',
    'Settings'                                 => 'Ustawienia',
    'Settings saved!'                          => 'Ustawienia zapisane!',
    'Sign up'                                  => 'Zaloguj się',
    'Social Login'                             => 'Social Login',
    'Social User'                              => 'Użytkownik Społecznościowy',
    'Social identities'                        => 'Tożsamości społecznościowe',
    'Social identities associated with this user'
 => 'Tożsamości społecznościowe powiązane z tym kontem',
    'Social login'                             => 'Logowanie społecznościowe',
    'Social login process uses <b>external service authority</b> to authenticate you.'
 => 'Logowanie społecznościowe używa <b>zewnętrznego dostawcy</b> aby Cię uwierzytelnić.',
    'Social user'                              => 'Użytkownik społecznościowy',
    'Successfully authenticated with social identity - :id (:type)'
 => 'Pomyślnie zweryfikowano tożsamość - :id (:type)',
    'Successfully connected social identity :id (:type) to your account!'
 => 'Pomyślnie podłączono tożsamość - :id (:type) do Twojego konta!',
    'Successfully disconnected social identity :id (:type) from your account!'
 => 'Pomyślnie odłączono tożsamość - :id (:type) od twojego konta!',
    'The list above shows users who filled up registration form but haven\'t yet confirmed their account.'
 => 'Lista powyżej ukazuje użytkowników, którzy wypełnili formularz rejestracji, ale nie aktywowali jeszcze swojego konta.',
    'There are errors in the submitted form'   => 'W formularzu znajdują się błędy',
    'There are no social services configured for connecting.'
 => 'Nie ma skonfigurowanych serwisów społecznościowych, które można podłączyć.',
    'There are no social services configured for login.'
 => 'Nie ma skonfigurowanych serwisów społecznościowych, z których można się zalogować.',
    'There are some errors in form'            => 'W formularzu są błędy',
    'This email already submitted registration.'
 => 'Ten email został już podany podczas rejestracji.',
    'This email is already pending registration! - :email'
 => 'Ten email już oczekuje na rejestrację! - :email',
    'This email is not registered in our site!'
 => 'Ten email nie znajduje się w bazie naszych użytkowników!',
    'This page is only accessible for logged in users.'
 => 'Ta strona jest dostępna tylko dla zalogowanych użytkowników.',
    'This username is already taken - :username'
 => 'Ta nazwa użytkownika jest już zajęta - :username',
    'To activate your account visit the following address:'
 => 'Aby aktywować Twoje konto odwiedź następujący adres:',
    'To connect or disconnect services create new Wolf page and set it\'s behavior to <b>Social profile</b>. Then go to this page in frontend.'
 => 'Aby podłączać i odłączać usługi utwórz nową stronę i ustaw jej typ na Social profile. Następnie odwiedź tę stronę.',
    'Turn on to see debug messages.'           => 'Włącz, aby widzieć wiadomości debugowania.',
    'Twitter'                                  => 'Twitter',
    'Type once again.'                         => 'Wpisz ponownie.',
    'Type the word from the image.'            => 'Wpisz słowo z obrazka.',
    'Type your username'                       => 'Wpisz nazwę użytkownika',
    'Type your username or email'              => 'Wpisz nazwę użytkownika lub email',
    'Unable to create DB table'                => 'Nie można utworzyć tabeli bazy danych',
    'Unable to disconnect social identity :id (:type) from your account'
 => 'Błąd podczas odłączania tożsamości :id (:type) od twojego konta!',
    'Unable to send email with new password! Try again later or contact administrator.'
 => 'Nie udało się przesłać wiadomości email z nowym hasłem! Spróbuj ponownie lub skontaktuj się z administratorem.',
    'Unreadable? Click the image to refresh.'  => 'Nieczytelny? Kliknij obrazek aby odświeżyć.',
    'Username'                                 => 'Nazwa użytkownika',
    'Username must consist of latin letters, digits and "-" or "_" characters!'
 => 'Nazwa użytkownika musi składać się z liter łacińskich oraz znaków "-" lub "_"',
    'Username must contain at least <b>:num characters</b>!'
 => 'Nazwa użytkownika musi zawierać przynajmniej <b>:num znaków</b>!',
    'We will send you an email with confirmation link to activate your new account.'
 => 'Wyślemy Ci email z linkiem aktywującym twoje nowe konto.',
    'Welcome again <b>:name</b>! You have successfully authenticated using </b>:type</b> '
 => 'Witaj ponownie <b>:name</b>! Pomyślnie uwierzytelniłeś się używając </b>:type</b>',
    'Welcome! You have successfully authenticated with social identity - :id (:type). New account :username has been created for you!'
 => 'Witamy! Zostałeś pomyślnie zidentyfikowany przy użyciu tożsamości społecznościowej :id (:type). Utworzyliśmy także nowe konto - :username!',
    'When you successfully authenticate, we will create account for you and automatically log you in.'
 => 'Po pomyślnym uwierzytelnieniu utworzymy dla Ciebie konto i zostaniesz zalogowany.',
    'Yes'                                      => 'Tak',
    'You can <b>connect your account</b> with the following social identities:'
 => 'Możesz <b>połączyć Twoje konto</b> z następującymi tożsamościami społecznościowymi:',
    'You can <b>register your new account</b> using email address.'
 => 'Możesz zarejestrować <b>nowe konto</b> używając adresu email.',
    'You can also use one of the following social services to login:'
 => 'Możesz także użyć jednego z następujących serwisów społecznościowych, aby się zalogować:',
    'You can login here using your username and password.'
 => 'Możesz się tutaj zalogować używając twojej nazwy użytkownika i hasła.',
    'You have logged in!'                      => 'Zostałeś zalogowany!',
    'You have logged out!'                     => 'Zostałeś wylogowany!',
    'You have modified this page.  If you navigate away from this page without first saving your data, the changes will be lost.'
 => 'Zmodyfikowałeś tę stronę. Jeśli ją opuścisz bez zapisywania zmian, zostaną one utracone.',
    'You have registered new account in :site_title.'
 => 'Zarejestrowałeś nowe konto w :site_title.',
    'You requested to access'                  => 'Zażądałeś dostępu do',
    'You will be redirected to your social service <b>login page</b> and asked to <b>grant permission</b> for this site.'
 => 'Zostaniesz przekierowany do <b>strony logowania</b> serwisu społecznościowego, a następnie poproszony o <b>przyznanie uprawnień</b> dla tego serwisu.',
    'Your account information'                 => 'Informacje o Twoim koncie',
    'Your account is <b>connected</b> to the following social identities:'
 => 'Twoje konto <b>jest połączone</b> z tymi tożsamościami społecznościowymi:',
    'Your account is <b>not connected</b> to any social identity.'
 => 'Twoje konto <b>nie jest połączone</b> z żadnymi tożsamościami społecznościowymi.',
    'Your avatar image'                        => 'Twój awatar',
    'Your name'                                => 'Twoje imię i nazwisko',
    'Your new password from '                  => 'Twoje nowe hasło z',
    'Your permissions'                         => 'Twoje uprawnienia',
    'Your roles'                               => 'Twoje role',
    'Your social identity was registered earlier but we could not find user account for your identity!'
 => 'Twoja tożsamość została zarejestrowana wcześniej, ale nie mogliśmy odnaleźć twojego konta!',
    'Your username'                            => 'Twoja nazwa użytkownika',
    'do not filter'                            => 'nie filtruj',
    'login'                                    => 'zaloguj',
    'or'                                       => 'lub',
    'same as above'                            => 'takie samo jak wyżej',
    'your name'                                => 'twoje imię i nazwisko',
    'your password'                            => 'twoje hasło',
    'your username'                            => 'twoja nazwa użytkownika',
    'your username or email'                   => 'twoja nazwa użytkownika lub email',
    'your@email.com'                           => 'twoj@email.pl',
);
