<?php

/**
 * English file for plugin social_login
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Your Name < email@domain.something >
 * @version Wolf 0.7.7
 */

return array(
    '-no roles assigned-'                      => '-no roles assigned-',
    '<b>:name</b> (:username) logged in'       => '<b>:name</b> (:username) logged in',
    'Account Settings'                         => 'Account Settings',
    'Account created on'                       => 'Account created on',
    'Actions'                                  => 'Actions',
    'An email has been sent with your new password!'
 => 'An email has been sent with your new password!',
    'Are you sure you want to delete it?'      => 'Are you sure you want to delete it?',
    'At least :num characters.'                => 'At least :num characters.',
    'Avatar images are associated to your e-mail address via gravatar.com'
 => 'Avatar images are associated to your e-mail address via gravatar.com',
    'Cannot use email: <b>:email</b> Other user already uses this email!'
 => 'Cannot use email: <b>:email</b> Other user already uses this email!',
    'Check your mailbox for confirmation email with link to activate your account!'
 => 'Check your mailbox for confirmation email with link to activate your account!',
    'Close'                                    => 'Close',
    'Confirm password'                         => 'Confirm password',
    'Connect :service to your account'         => 'Connect :service to your account',
    'Could not obtain user data from OAuth provider - :type'
 => 'Could not obtain user data from OAuth provider - :type',
    'Create new account'                       => 'Create new account',
    'Currently available roles'                => 'Currently available roles',
    'DB error while authenticating with social identity - :id (:type)'
 => 'DB error while authenticating with social identity - :id (:type)',
    'DB error while creating new account for social identity - :id (:type)'
 => 'DB error while creating new account for social identity - :id (:type)',
    'Data provided'                            => 'Data provided',
    'Debug mode'                               => 'Debug mode',
    'Delete identity'                          => 'Delete identity',
    'Delete pending registration'              => 'Delete pending registration',
    'Deleted identity <b>:id</b>'              => 'Deleted identity <b>:id</b>',
    'Deleted pending registration <b>:name</b>'
 => 'Deleted pending registration <b>:name</b>',
    'Disconnect :service from your account'    => 'Disconnect :service from your account',
    'Documentation'                            => 'Documentation',
    'E-mail'                                   => 'E-mail',
    'E-mail in service'                        => 'E-mail in service',
    'Email'                                    => 'Email',
    'Email in service'                         => 'Email in service',
    'Email registration is off.'               => 'Email registration is off.',
    'Error connecting social identity :id (:type) to your account!'
 => 'Error connecting social identity :id (:type) to your account!',
    'Error creating new account. Please contact administrator!'
 => 'Error creating new account. Please contact administrator!',
    'Error creating your account!'             => 'Error creating your account!',
    'Error deleting identity of <b>:id</b>'    => 'Error deleting identity of <b>:id</b>',
    'Error deleting pending registration of <b>:name</b>'
 => 'Error deleting pending registration of <b>:name</b>',
    'Error generating new password!'           => 'Error generating new password!',
    'Error saving changes!'                    => 'Error saving changes!',
    'Error saving settings!'                   => 'Error saving settings!',
    'Error while logging you in!'              => 'Error while logging you in!',
    'Facebook'                                 => 'Facebook',
    'Fill the fields below to change password for your account.'
 => 'Fill the fields below to change password for your account.',
    'Filter by Username'                       => 'Filter by Username',
    'Filter by service'                        => 'Filter by service',
    'Forgot password?'                         => 'Forgot password?',
    'General settings'                         => 'General settings',
    'Github'                                   => 'Github',
    'Go to backend'                            => 'Go to backend',
    'Go to homepage'                           => 'Go to homepage',
    'Go to login page'                         => 'Go to login page',
    'Google'                                   => 'Google',
    'Hello'                                    => 'Hello',
    'Hello, :name'                             => 'Hello, :name',
    'ID'                                       => 'ID',
    'ID in service'                            => 'ID in service',
    'If you did not submit account registration form, please ignore this email.'
 => 'If you did not submit account registration form, please ignore this email.',
    'If you forgot your password, you can reset it here by providing your e-mail address.'
 => 'If you forgot your password, you can reset it here by providing your e-mail address.',
    'If you leave your password empty you will only be able to login to this site with social identities attached to your accout.'
 => 'If you leave your password empty you will only be able to login to this site with social identities attached to your accout.',
    'Invalid email address!'                   => 'Invalid email address!',
    'Invalid security check. Try again!'       => 'Invalid security check. Try again!',
    'JSON data preview'                        => 'JSON data preview',
    'Last failed login'                        => 'Last failed login',
    'Last login'                               => 'Last login',
    'Login'                                    => 'Login',
    'Login failed. Please check your login data and try again.'
 => 'Login failed. Please check your login data and try again.',
    'Login successful!'                        => 'Login successful!',
    'Logo'                                     => 'Logo',
    'Logout'                                   => 'Logout',
    'Must be valid.'                           => 'Must be valid.',
    'Name'                                     => 'Name',
    'Name in service'                          => 'Name in service',
    'Name must contain at least <b>:num characters</b>!'
 => 'Name must contain at least <b>:num characters</b>!',
    'New User <b>:name</b> logged in with new social identity (:type).'
 => 'New User <b>:name</b> logged in with new social identity (:type).',
    'New account registration in :site_title'  => 'New account registration in :site_title',
    'New password'                             => 'New password',
    'New password confirm'                     => 'New password confirm',
    'No'                                       => 'No',
    'No POST data!'                            => 'No POST data!',
    'No data sent!'                            => 'No data sent!',
    'Note: <b>Your password is empty</b>. It seems your account was created using social login.'
 => 'Note: <b>Your password is empty</b>. It seems your account was created using social login.',
    'Note: You can hold SHIFT to delete instanty'
 => 'Note: You can hold SHIFT to delete instanty',
    'OAuth authorization_error - :type'        => 'OAuth authorization_error - :type',
    'OAuth authorization_error - :type - :error'
 => 'OAuth authorization_error - :type - :error',
    'Off'                                      => 'Off',
    'Offline access'                           => 'Offline access',
    'On'                                       => 'On',
    'Optional. If set, should be valid.'       => 'Optional. If set, should be valid.',
    'Password'                                 => 'Password',
    'Password and Confirm were <b>not the same</b> or <b>too short</b>!'
 => 'Password and Confirm were <b>not the same</b> or <b>too short</b>!',
    'Password and confirmation are not the same!'
 => 'Password and confirmation are not the same!',
    'Password cannot be the same as the <b>username</b>!'
 => 'Password cannot be the same as the <b>username</b>!',
    'Password cannot be the same as the username!'
 => 'Password cannot be the same as the username!',
    'Password must contain at least <b>:num characters</b>!'
 => 'Password must contain at least <b>:num characters</b>!',
    'Pending email registrations'              => 'Pending email registrations',
    'Please provide password confirm!'         => 'Please provide password confirm!',
    'Please provide password!'                 => 'Please provide password!',
    'Please provide your email address!'       => 'Please provide your email address!',
    'Please your name!'                        => 'Please your name!',
    'Please your username!'                    => 'Please your username!',
    'Provides OAuth social login and account management. (Google, Facebook, Twitter)'
 => 'Provides OAuth social login and account management. (Google, Facebook, Twitter)',
    'Register by e-mail'                       => 'Register by e-mail',
    'Register new account'                     => 'Register new account',
    'Register user'                            => 'Register user',
    'Registration date'                        => 'Registration date',
    'Request password reset'                   => 'Request password reset',
    'Save'                                     => 'Save',
    'Save account settings'                    => 'Save account settings',
    'Saved changes!'                           => 'Saved changes!',
    'Security check'                           => 'Security check',
    'See provided JSON data'                   => 'See provided JSON data',
    'Send password'                            => 'Send password',
    'Service ID'                               => 'Service ID',
    'Service name'                             => 'Service name',
    'Service type'                             => 'Service type',
    'Set your new password to be able to login using username <b>:username</b> and your new password.'
 => 'Set your new password to be able to login using username <b>:username</b> and your new password.',
    'Settings'                                 => 'Settings',
    'Settings saved!'                          => 'Settings saved!',
    'Sign up'                                  => 'Sign up',
    'Social Login'                             => 'Social Login',
    'Social User'                              => 'Social User',
    'Social identities'                        => 'Social identities',
    'Social identities associated with this user'
 => 'Social identities associated with this user',
    'Social login'                             => 'Social login',
    'Social login process uses <b>external service authority</b> to authenticate you.'
 => 'Social login process uses <b>external service authority</b> to authenticate you.',
    'Social user'                              => 'Social user',
    'Successfully authenticated with social identity - :id (:type)'
 => 'Successfully authenticated with social identity - :id (:type)',
    'Successfully connected social identity :id (:type) to your account!'
 => 'Successfully connected social identity :id (:type) to your account!',
    'Successfully disconnected social identity :id (:type) from your account!'
 => 'Successfully disconnected social identity :id (:type) from your account!',
    'The list above shows users who filled up registration form but haven\'t yet confirmed their account.'
 => 'The list above shows users who filled up registration form but haven\'t yet confirmed their account.',
    'There are errors in the submitted form'   => 'There are errors in the submitted form',
    'There are no social services configured for connecting.'
 => 'There are no social services configured for connecting.',
    'There are no social services configured for login.'
 => 'There are no social services configured for login.',
    'There are some errors in form'            => 'There are some errors in form',
    'This email already submitted registration.'
 => 'This email already submitted registration.',
    'This email is already pending registration! - :email'
 => 'This email is already pending registration! - :email',
    'This email is not registered in our site!'
 => 'This email is not registered in our site!',
    'This page is only accessible for logged in users.'
 => 'This page is only accessible for logged in users.',
    'This username is already taken - :username'
 => 'This username is already taken - :username',
    'To activate your account visit the following address:'
 => 'To activate your account visit the following address:',
    'To connect or disconnect services create new Wolf page and set it\''
 => 'To connect or disconnect services create new Wolf page and set it\'',
    'Turn on to see debug messages.'           => 'Turn on to see debug messages.',
    'Twitter'                                  => 'Twitter',
    'Type once again.'                         => 'Type once again.',
    'Type the word from the image.'            => 'Type the word from the image.',
    'Type your username'                       => 'Type your username',
    'Type your username or email'              => 'Type your username or email',
    'Unable to create DB table'                => 'Unable to create DB table',
    'Unable to disconnect social identity :id (:type) from your account'
 => 'Unable to disconnect social identity :id (:type) from your account',
    'Unable to send email with new password! Try again later or contact administrator.'
 => 'Unable to send email with new password! Try again later or contact administrator.',
    'Unreadable? Click the image to refresh.'  => 'Unreadable? Click the image to refresh.',
    'User <b>:name</b> connected new social identity (:type) to his account.'
 => 'User <b>:name</b> connected new social identity (:type) to his account.',
    'User <b>:name</b> disconnected social identity (:type) from his account.'
 => 'User <b>:name</b> disconnected social identity (:type) from his account.',
    'User <b>:name</b> logged in with new social identity (:type).'
 => 'User <b>:name</b> logged in with new social identity (:type).',
    'User <b>:name</b> logged in.'             => 'User <b>:name</b> logged in.',
    'Username'                                 => 'Username',
    'Username must consist of latin letters, digits and "-" or "_" characters!'
 => 'Username must consist of latin letters, digits and "-" or "_" characters!',
    'Username must contain at least <b>:num characters</b>!'
 => 'Username must contain at least <b>:num characters</b>!',
    'We will send you an email with confirmation link to activate your new account.'
 => 'We will send you an email with confirmation link to activate your new account.',
    'Welcome again <b>:name</b>! You have successfully authenticated using </b>:type</b> '
 => 'Welcome again <b>:name</b>! You have successfully authenticated using </b>:type</b> ',
    'Welcome! You have successfully authenticated with social identity - :id (:type). New account :username has been created for you!'
 => 'Welcome! You have successfully authenticated with social identity - :id (:type). New account :username has been created for you!',
    'When you successfully authenticate, we will create account for you and automatically log you in.'
 => 'When you successfully authenticate, we will create account for you and automatically log you in.',
    'Yes'                                      => 'Yes',
    'You can <b>connect your account</b> with the following social identities:'
 => 'You can <b>connect your account</b> with the following social identities:',
    'You can <b>register your new account</b> using email address.'
 => 'You can <b>register your new account</b> using email address.',
    'You can also use one of the following social services to login:'
 => 'You can also use one of the following social services to login:',
    'You can login here using your username and password.'
 => 'You can login here using your username and password.',
    'You have logged in!'                      => 'You have logged in!',
    'You have logged out!'                     => 'You have logged out!',
    'You have modified this page.  If you navigate away from this page without first saving your data, the changes will be lost.'
 => 'You have modified this page.  If you navigate away from this page without first saving your data, the changes will be lost.',
    'You have registered new account in :site_title.'
 => 'You have registered new account in :site_title.',
    'You requested to access'                  => 'You requested to access',
    'You will be redirected to your social service <b>login page</b> and asked to <b>grant permission</b> for this site.'
 => 'You will be redirected to your social service <b>login page</b> and asked to <b>grant permission</b> for this site.',
    'Your account information'                 => 'Your account information',
    'Your account is <b>connected</b> to the following social identities:'
 => 'Your account is <b>connected</b> to the following social identities:',
    'Your account is <b>not connected</b> to any social identity.'
 => 'Your account is <b>not connected</b> to any social identity.',
    'Your avatar image'                        => 'Your avatar image',
    'Your name'                                => 'Your name',
    'Your new password from '                  => 'Your new password from ',
    'Your permissions'                         => 'Your permissions',
    'Your roles'                               => 'Your roles',
    'Your social identity was registered earlier but we could not find user account for your identity!'
 => 'Your social identity was registered earlier but we could not find user account for your identity!',
    'Your username'                            => 'Your username',
    'do not filter'                            => 'do not filter',
    'login'                                    => 'login',
    'or'                                       => 'or',
    'same as above'                            => 'same as above',
    'your name'                                => 'your name',
    'your password'                            => 'your password',
    'your username'                            => 'your username',
    'your username or email'                   => 'your username or email',
    'your@email.com'                           => 'your@email.com',
);
