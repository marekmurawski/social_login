<?php

/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') ) {
    exit();
}


class UserSocial extends Record {

    const TABLE_NAME = 'user_social';

    public $id;
    public $user_id;
    public $service_type;
    public $refresh_token;
    public $social_id;
    public $social_name;
    public $social_email;
    public $json;
    // OAuth object variable
    private $oauth;

    public static function find($args = null) {

        // Collect attributes...
        $where     = isset($args['where']) ? trim($args['where']) : '';
        $order_by  = isset($args['order']) ? trim($args['order']) : '';
        $offset    = isset($args['offset']) ? (int) $args['offset'] : 0;
        $limit     = isset($args['limit']) ? (int) $args['limit'] : 0;
        $with_user = isset($args['with_user']) ? (bool) $args['with_user'] : false;
        $debug     = isset($args['debug']) && ($args['debug'] > 0);

        // Prepare query parts
        $where_string    = empty($where) ? '' : "WHERE $where";
        $order_by_string = empty($order_by) ? '' : "ORDER BY $order_by";
        $limit_string    = $limit > 0 ? "LIMIT $limit" : '';
        $offset_string   = $offset > 0 ? "OFFSET $offset" : '';

        $tablename      = self::tableNameFromClassName('UserSocial');
        $user_tablename = self::tableNameFromClassName('User');

        // Prepare SQL
        $sql = "SELECT $tablename.* ";
        if ( $with_user )
            $sql .= ",";
        $sql .= "
        $user_tablename.username AS user_username,
        $user_tablename.name AS user_name
        " . PHP_EOL;
        $sql .= " FROM $tablename " . PHP_EOL;
        if ( $with_user )
            $sql .= " LEFT JOIN $user_tablename ON $tablename.user_id = $user_tablename.id" . PHP_EOL;
        $sql .= " $where_string $order_by_string $limit_string $offset_string";

        Record::logQuery($sql);

        //if ( $debug )
        //    echo '<pre>' . print_r($sql, true) . '</pre>';

        $stmt = self::$__CONN__->prepare($sql);
        $stmt->execute();

        // Run!
        if ( $limit == 1 ) {
            return $stmt->fetchObject('UserSocial');
        } else {
            $objects = array( );
            while ( $object  = $stmt->fetchObject('UserSocial') ) {
                $objects[] = $object;
            }
            return $objects;
        }

    }


    public static function findByService($service_type, $social_id) {
        return self::findOneFrom('UserSocial', 'service_type=:st AND social_id=:sid', array(
                                ':st'  => $service_type,
                                ':sid' => $social_id,
        ));

    }


    public static function findByWolfUserId($user_id) {
        return self::findAllFrom('UserSocial', 'user_id=:id', array(
                                ':id' => $user_id,
        ));

    }


    public function __construct($service = false) {

        if ( $service ) {
            if ( in_array($service, SocialLoginSettings::$services) )
                $this->service_type = $service;
        }
        $this->oauth = new oauth_client_class;

        // Oauth library predefined servers have first letter uppercase
        $this->oauth->server = ucfirst($service); //isset($settings['server']) ? $settings['server'] : false;

        if ( !empty($service) )
            $this->oauth->client_id = SocialLoginSettings::get($service . '_client_id');

        if ( !empty($service) )
            $this->oauth->client_secret = SocialLoginSettings::get($service . '_client_secret');

        $this->oauth->offline      = (bool) SocialLoginSettings::get($service . '_offline');
        $this->oauth->redirect_uri = URL_PUBLIC . SOCIAL_FRONTEND_URI . '/' . $service;


        if ( $scope = SocialLoginSettings::get($service . '_scope') ) {
            $this->oauth->scope = $scope;
        }

        $this->oauth->debug      = SocialLoginSettings::get('debug_mode');
        $this->oauth->debug_http = SocialLoginSettings::get('debug_mode');

    }


    private function addNewWolfUser($userInfo) {

        $newWolfUser = new User();

        $newWolfUser->name  = $userInfo->name;
        $newWolfUser->email = $userInfo->email;

        $baseUsername = Node::toSlug($userInfo->name);
        $newUsername  = $baseUsername;
        $increment    = 1;

        /**
         * Find free username by incrementally adding number
         */
        while ( Record::existsIn('User', 'username=:uname', array( ':uname' => $newUsername )) ) {
            $newUsername = $baseUsername . $increment;
            $increment++;
        }

        $newWolfUser->username = $newUsername;

        if ( $newWolfUser->save() ) {
            /**
             * Add Role to user
             */
            //UserRole::setRolesFor($user_id, $roles);

            if ( SOCIAL_DEBUG )
                echo 'DEBUG: Created new Wolf User account - ' . $newWolfUser->username . '<br/>' . PHP_EOL;

            $newUserRolesStr = trim(SocialLoginSettings::get('newuser_roles'), ', ');
            $newUserRolesArr = explode(',', $newUserRolesStr);
            foreach ( $newUserRolesArr as $newRoleStr ) {
                // cleanup
                $newRoleStr = trim($newRoleStr);
                if ( $role       = Role::findByName($newRoleStr) ) {
                    if ( version_compare(CMS_VERSION, '0.7.5', '<=') ) {
                        UserRole::setPermissionsFor($newWolfUser->id, array( $role->id )); // WOLF 0.7.5 uses this
                        if ( SOCIAL_DEBUG )
                            echo 'DEBUG: Role <b>' . $newRoleStr . '</b> added for user <b>' . $newWolfUser->name . '</b><br/>';
                    } else {
                        UserRole::setRolesFor($newWolfUser->id, array( $role->id )); // WOLF 0.7.6 and later
                        if ( SOCIAL_DEBUG )
                            echo 'DEBUG: Role <b>' . $newRoleStr . '</b> added for user <b>' . $newWolfUser->name . '</b><br/>';
                    }
                }
            }
            //echo '<pre>' . print_r($newUserRolesArr, true) . '</pre>';
            //die($newUserRolesStr);

            return $newWolfUser;
        }
        return false;

    }


    public function logout() {
        //$this->init_oauth($service_type);
        //echo '<pre>' . print_r($this->oauth, true) . '</pre>';
        echo 'Logging out';
        if ( ($success = $this->oauth->Initialize() ) ) {
            echo 'process';
            $this->oauth->ResetAccessToken();
            //echo $this->oauth->access_token_url;
        }
        $success = $this->oauth->Finalize($success);

        //echo '<pre>' . print_r($this->oauth, true) . '</pre>';
        $this->oauth->Output();
        //unset($_SESSION['OAUTH_ACCESS_TOKEN']);
        //unset($_SESSION['OAUTH_STATE']);
        //AuthUser::logout();
        //redirect('/');

    }


    public function disconnect() {
        $wolfUser = AuthUser::getRecord();
        if ( $this->delete() ) {
            Flash::setNow('UserSocial', $this);
            Flash::setNow('User', $wolfUser);
            Flash::setNow('usersocial_success', __('Successfully disconnected social identity :id (:type) from your account!', array(
                        ':id'   => $this->social_id,
                        ':type' => $this->service_type
            )));
            Observer::notify('log_event', __('User <b>:name</b> disconnected social identity (:type) from his account.', array(
                        ':name' => $wolfUser->username,
                        ':type' => $this->service_type
                                    ), 'social_login'));
            return true;
        } else {
            Flash::setNow('usersocial_error', __('Unable to disconnect social identity :id (:type) from your account', array(
                        ':id'   => $this->social_id,
                        ':type' => $this->service_type
            )));
            return false;
        }

    }


    public function connect() {

        if ( SOCIAL_DEBUG )
            echo 'DEBUG: UserSocial->connect() called' . '<br/>';
        if ( ($success = $this->oauth->Initialize() ) ) {
            if ( ($success = $this->oauth->Process() ) ) {
                if ( strlen($this->oauth->authorization_error) ) {
                    $this->oauth->error = $this->oauth->authorization_error;
                    $success            = false;
                    Flash::setNow('usersocial_error', __('OAuth authorization_error - :type', array(
                                ':type' => $this->service_type
                    )));
                    return false;
                } elseif ( strlen($this->oauth->access_token) ) {
                    // use predefined methods to retrieve social account data
                    $method     = 'call_api_info_' . $this->service_type;
                    $socialInfo = $this->$method();
                }
            }
            $success = $this->oauth->Finalize($success);
        }

        if ( $this->oauth->exit )
            exit;

        if ( !$socialInfo ) {
            Flash::setNow('usersocial_error', __('Could not obtain user data from OAuth provider - :type', array(
                        ':type' => $this->service_type
            )));
            // echo '<pre>' . print_r($this->oauth, true) . '</pre>';
            //$this->oauth->Output();
            return false;
        }

        if ( SOCIAL_DEBUG )
            echo 'DEBUG: UserSocial->connect() ' . '<br/>' . PHP_EOL;
        //echo '<pre>' . print_r($socialInfo, true) . '</pre>';

        if ( $success ) {
            $userSocial = self::findByService($this->service_type, $socialInfo->id);

            /**
             * STORE REFRESH TOKEN
             */
            if ( $this->refresh_token !== $this->oauth->refresh_token ) {
                $this->refresh_token = $this->oauth->refresh_token;
                if ( SOCIAL_DEBUG )
                    echo 'DEBUG: New REFRESH_TOKEN obtained: ' . $this->refresh_token . '<br/>' . PHP_EOL;
            }


            if ( $userSocial ) { // Social user exists! Logging in
                if ( SOCIAL_DEBUG ) {
                    echo 'DEBUG: Social account found!' . '<br/>' . PHP_EOL;
                    //echo '<pre>' . print_r($userSocial, true) . '</pre>';
                }
            } else {
                if ( SOCIAL_DEBUG )
                    echo 'DEBUG: Social NOT found! Creating new UserSocial' . '<br/>' . PHP_EOL;
                $userSocial                = new UserSocial($this->service_type);
                $userSocial->service_type  = $this->service_type;
                $userSocial->service_token = $this->service_token;
                $userSocial->social_id     = $socialInfo->id;
                $userSocial->social_name   = $socialInfo->name;
                $userSocial->social_email  = $socialInfo->email;
                $userSocial->json          = json_encode($socialInfo);
                if ( SOCIAL_DEBUG ) {
                    echo '<pre>' . print_r($userSocial, true) . '</pre>';
                }
            }

            if ( $otherWolfUser = User::findById($userSocial->user_id) ) {
                // another Wolf user has this Identity!!!!!
                if ( $otherWolfUser->id !== AuthUser::getId() ) {
                    if ( SOCIAL_DEBUG )
                        echo 'DEBUG: Another user has this identity! - ' . $otherWolfUser->name . ' - username: ' . $otherWolfUser->username . '<br/>' . PHP_EOL;
                }
            }

            $wolfUser = AuthUser::getRecord();
            if ( SOCIAL_DEBUG ) {
                echo 'DEBUG: No other user has this identity!' . '<br/>' . PHP_EOL;
                //echo '<pre>' . print_r($wolfUser, true) . '</pre>';
            }

            if ( $wolfUser ) {
                /**
                 * Associate new UserSocial to User
                 */
                $userSocial->user_id = $wolfUser->id;

                // Store new UserSocial
                if ( $userSocial->save() ) {
                    AuthUser::forceLogin($wolfUser->username, true);
                    Observer::notify('log_event', __('User <b>:name</b> connected new social identity (:type) to his account.', array(
                                ':name' => $wolfUser->username,
                                ':type' => $userSocial->service_type
                                            ), 'social_login'));
                    Flash::setNow('UserSocial', $userSocial);
                    Flash::setNow('User', $wolfUser);
                    Flash::setNow('usersocial_success', __('Successfully connected social identity :id (:type) to your account!', array(
                                ':id'   => $this->social_id,
                                ':type' => $this->service_type
                    )));

                    return true;
                } else {
                    Flash::setNow('usersocial_error', __('Error connecting social identity :id (:type) to your account!', array(
                                ':id'   => $this->social_id,
                                ':type' => $this->service_type
                    )));
                    return false;
                }
            } else {
                die('AuthUser not found!');
            }
        } else {
            echo '<pre>' . HtmlSpecialChars($this->oauth->error) . '</pre>';
        }

    }


    public function try_login() {

        if ( SOCIAL_DEBUG )
            echo 'DEBUG: UserSocial->try_login() called' . '<br/>';
        if ( ($success = $this->oauth->Initialize() ) ) {
            // LOGIN HANDLING
            if ( ($success = $this->oauth->Process() ) ) {
                if ( SOCIAL_DEBUG )
                    echo 'DEBUG: oauth->Process() done' . '<br/>';

                // echo '<pre>' . print_r($this->oauth, true) . '</pre>';

                if ( strlen($this->oauth->authorization_error) ) {
                    $this->oauth->error = $this->oauth->authorization_error;
                    $success            = false;
                    Flash::setNow('usersocial_error', __('OAuth authorization_error - :type', array(
                                ':type' => $this->service_type
                    )));
                    return false;
                } elseif ( strlen($this->oauth->access_token) ) {
                    // use predefined methods to retrieve social account data
                    $method     = 'call_api_info_' . $this->service_type;
                    $socialInfo = $this->$method();
                    // echo 'call_api_info_' . $service_type . ' done!' . '<br/>';
                }
            }
            $success = $this->oauth->Finalize($success);
        }

        if ( $this->oauth->exit )
            exit;

        if ( !$socialInfo ) {
            Flash::setNow('usersocial_error', __('Could not obtain user data from OAuth provider - :type', array(
                        ':type' => $this->service_type
            )));
            // echo '<pre>' . print_r($this->oauth, true) . '</pre>';
            $this->oauth->Output();
            return false;
        }

        // echo '<pre>' . print_r($socialInfo, true) . '</pre>';
        if ( SOCIAL_DEBUG )
            echo 'DEBUG: UserSocial->try_login() function' . '<br/>' . PHP_EOL;

        if ( $success ) {
            $userSocial = self::findByService($this->service_type, $socialInfo->id);

            /**
             * STORE REFRESH TOKEN
             */
            if ( $this->refresh_token !== $this->oauth->refresh_token ) {
                $this->refresh_token = $this->oauth->refresh_token;
                if ( SOCIAL_DEBUG )
                    echo 'DEBUG: New REFRESH_TOKEN obtained: ' . $this->refresh_token . '<br/>' . PHP_EOL;
            }

            if ( $userSocial ) { // Social user exists! Logging in
                if ( SOCIAL_DEBUG ) {
                    echo 'DEBUG: Social account found!' . '<br/>' . PHP_EOL;
                    //echo '<pre>' . print_r($userSocial, true) . '</pre>';
                }
                if ( $returningWolfUser = User::findById($userSocial->user_id) ) {
                    if ( AuthUser::forceLogin($returningWolfUser->username) ) {
                        Observer::notify('log_event', __('User <b>:name</b> logged in.', array( ':name' => $returningWolfUser->username )), 'social_login');
                        if ( SOCIAL_DEBUG )
                            echo('DEBUG: Logged in user id = ' . $returningWolfUser->id . ', username = ' . $returningWolfUser->username . '<br/>');
                        Flash::setNow('UserSocial', $userSocial);
                        Flash::setNow('User', $returningWolfUser);
                        Flash::setNow('usersocial_success', __('Welcome again <b>:name</b>! You have successfully authenticated using </b>:type</b> ', array(
                                    ':name' => $returningWolfUser->name,
                                    ':type' => $userSocial->service_type,
                        )));
                        return true;
                    } else {
                        Flash::setNow('usersocial_error', __('Error while logging you in!'));
                        return false;
                    }
                } else {
                    Flash::setNow('usersocial_error', __('Your social identity was registered earlier but we could not find user account for your identity!'));
                    return false;
                }
            } else {
                /**
                 * Existing UserSocial NOT FOUND!
                 */
                /**
                 * Creating new UserSocial identity
                 * @TODO $NEWUSERSOCIAL -> $THIS
                 */
                if ( SOCIAL_DEBUG )
                    echo 'DEBUG: Social NOT found! Creating new UserSocial' . '<br/>' . PHP_EOL;
                $newUserSocial                = new UserSocial($this->service_type);
                $newUserSocial->service_type  = $this->service_type;
                $newUserSocial->refresh_token = $this->refresh_token;
                $newUserSocial->social_id     = $socialInfo->id;
                $newUserSocial->social_name   = $socialInfo->name;
                $newUserSocial->social_email  = $socialInfo->email;
                $newUserSocial->json          = json_encode($socialInfo);
                //if ( SOCIAL_DEBUG ) {
                //    echo '<pre>' . print_r($newUserSocial, true) . '</pre>';
                //}

                /**
                 * Trying to find existing Wolf User
                 * with the same email and associate
                 * newly created identity to him,
                 * then login.
                 *
                 */
                if ( $existingWolfUser = User::findBy('email', $socialInfo->email) ) {
                    if ( SOCIAL_DEBUG )
                        echo 'DEBUG: Found Wolf user with email: ' . $socialInfo->email . '<br/>' . PHP_EOL;
                    /**
                     * Associate new UserSocial to Wolf User
                     * and store new UserSocial.
                     */
                    $newUserSocial->user_id = $existingWolfUser->id;

                    if ( $newUserSocial->save() ) {
                        AuthUser::forceLogin($existingWolfUser->username, true);
                        Observer::notify('log_event', __('User <b>:name</b> logged in with new social identity (:type).', array(
                                    ':name' => $existingWolfUser->username,
                                    ':type' => $newUserSocial->service_type
                                                ), 'social_login'));
                        Flash::setNow('UserSocial', $newUserSocial);
                        Flash::setNow('User', $existingWolfUser);
                        Flash::setNow('usersocial_success', __('Successfully authenticated with social identity - :id (:type)', array(
                                    ':id'   => $newUserSocial->social_id,
                                    ':type' => $newUserSocial->service_type
                        )));
                        return true;
                    } else {
                        Flash::setNow('usersocial_error', __('DB error while authenticating with social identity - :id (:type)', array(
                                    ':id'   => $newUserSocial->social_id,
                                    ':type' => $newUserSocial->service_type
                        )));
                        return false;
                    }
                } else {
                    /**
                     * Existing Wolf User was not found,
                     *
                     * Trying to create new Wolf User
                     * and associate newly created identity
                     * to his account, then login.
                     */
                    if ( SOCIAL_DEBUG )
                        echo 'DEBUG: Wolf user with email: ' . $socialInfo->email . ' not found! - Will try to create new Wolf User.<br/>' . PHP_EOL;
                    $newWolfUser = self::addNewWolfUser($socialInfo, 'en');
                    if ( $newWolfUser ) {
                        AuthUser::forceLogin($newWolfUser->username, true);

                        /**
                         * Associate new UserSocial to User
                         */
                        $newUserSocial->user_id = $newWolfUser->id;

                        // store new UserSocial
                        if ( $newUserSocial->save() ) {
                            if ( SOCIAL_DEBUG )
                                echo 'DEBUG: Stored new UserSocial record - ' . $newUserSocial->social_id . '<br/>' . PHP_EOL;
                            AuthUser::forceLogin($newWolfUser->username, true);
                            Observer::notify('log_event', __('New User <b>:name</b> logged in with new social identity (:type).', array(
                                        ':name' => $newWolfUser->username,
                                        ':type' => $newUserSocial->service_type
                                                    ), 'social_login'));
                            Flash::setNow('UserSocial', $newUserSocial);
                            Flash::setNow('User', $newWolfUser);
                            Flash::setNow('usersocial_success', __('Welcome! You have successfully authenticated with social identity - :id (:type). New account :username has been created for you!', array(
                                        ':id'       => $newUserSocial->social_id,
                                        ':type'     => $newUserSocial->service_type,
                                        ':username' => $newWolfUser->username,
                            )));
                            return true;
                        } else {
                            Flash::setNow('usersocial_error', __('DB error while authenticating with social identity - :id (:type)', array(
                                        ':id'   => $newUserSocial->social_id,
                                        ':type' => $newUserSocial->service_type
                            )));
                            return false;
                        }
                    } else {
                        Flash::setNow('usersocial_error', __('DB error while creating new account for social identity - :id (:type)', array(
                                    ':id'   => $newUserSocial->social_id,
                                    ':type' => $newUserSocial->service_type
                        )));
                        return false;
                    }
                }
            }
        } else {
            if ( SOCIAL_DEBUG )
                echo '<pre>' . HtmlSpecialChars($this->oauth->error) . '</pre>';
        }

    }


    public function callApi($url, $method = 'GET', $parameters = array( ), $options = array( 'FailOnAccessError' => true )) {
        //$this->init_oauth($service_type);
        //echo '_init_oauth done' . '<br/>';

        if ( ($success = $this->oauth->Initialize() ) ) {
            echo 'oauth->Initialize() done' . '<br/>';
            // LOGIN HANDLING
            if ( ($success = $this->oauth->Process() ) ) {
                echo 'oauth->Process() done' . '<br/>';
                //echo '<pre>' . print_r($this->oauth, true) . '</pre>';
                if ( strlen($this->oauth->authorization_error) ) {
                    $this->oauth->error = $this->oauth->authorization_error;
                    $success            = false;
                    Flash::setNow('usersocial_error', __('OAuth authorization_error - :type - :error', array(
                                ':type'  => $this->service_type,
                                ':error' => $this->oauth->authorization_error,
                    )));
                    return false;
                } elseif ( strlen($this->oauth->access_token) ) {
                    $success = $this->oauth->CallAPI($url, $method, $parameters, $options, $info);
                }
            }
            $success = $this->oauth->Finalize($success);
        }
        if ( $this->oauth->exit )
            exit;

        if ( $success )
            return $info;
        return false;

    }


    public function getOAuth() {
        return $this->oauth;

    }


    public function getOAuthToken() {
        if ( ($success = $this->oauth->Initialize() ) ) {
            //echo 'oauth->Initialize() done' . '<br/>';
            // LOGIN HANDLING
            if ( ($success = $this->oauth->Process() ) ) {
                echo 'oauth->Process() done' . '<br/>';
                //echo '<pre>' . print_r($this->oauth, true) . '</pre>';
                if ( strlen($this->oauth->authorization_error) ) {
                    $this->oauth->error = $this->oauth->authorization_error;
                    $success            = false;
                    Flash::setNow('usersocial_error', __('OAuth authorization_error - :type - :error', array(
                                ':type'  => $this->service_type,
                                ':error' => $this->oauth->authorization_error,
                    )));
                    return false;
                } elseif ( strlen($this->oauth->access_token) ) {
                    return $this->oauth->access_token;
                    //echo '<pre>' . print_r($this->oauth->access_token, true) . '</pre>';
                    //die();
                    //$success = $this->oauth->CallAPI($url, $method, $parameters, $options, $info);
                }
            }
            $success = $this->oauth->Finalize($success);
        }
        if ( $this->oauth->exit )
            exit;

        if ( $success )
            return $info;
        return false;

    }


    public function revoke_access($service_type) {
        $this->call_api($service_type, 'https://accounts.google.com/o/oauth2/revoke?token={token}');

    }


    private function call_api_info_google() {
        if ( $this->oauth->CallAPI('https://www.googleapis.com/oauth2/v1/userinfo', 'GET', array( ), array( 'FailOnAccessError' => true ), $info) ) {
            return $info;
        }
        else
            return false;

    }


    private function call_api_info_facebook() {
        if ( $this->oauth->CallAPI('https://graph.facebook.com/me', 'GET', array( ), array( 'FailOnAccessError' => true ), $info) ) {
            return $info;
        }
        else
            return false;

    }


    private function call_api_info_twitter() {
        if ( $this->oauth->CallAPI('https://api.twitter.com/1.1/account/verify_credentials.json', 'GET', array( ), array( 'FailOnAccessError' => true ), $info) ) {
            // twitter doesn't provide email
            $info->email = '';
            return $info;
        }
        else
            return false;

    }


    private function call_api_info_github() {
        //if ( $this->oauth->CallAPI('https://api.github.com/user', 'GET', array( ), array( 'FailOnAccessError' => true ), $info) ) {
        if ( $this->oauth->CallAPI('https://api.github.com/user', 'GET', array( ), array( 'FailOnAccessError' => true ), $info) ) {
            // github doesn't provide name
            // we'll use login instead
            $info->name = $info->login;
            return $info;
        }
        else
            return false;

    }


}