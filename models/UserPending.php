<?php

/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') )
    exit();


class UserPending extends Record {

    const TABLE_NAME = 'user_pending';

    public $id;
    public $name;
    public $username;
    public $email;
    public $password;
    public $reg_hash;
    public $reg_salt;
    public $reg_date;

    public function getColumns() {
        return array(
                    'id',
                    'name',
                    'username',
                    'email',
                    'password',
                    'reg_hash',
                    'reg_salt',
                    'reg_date',
        );

    }


    public static function findAll() {
        return Record::findAllFrom('UserPending');

    }


}