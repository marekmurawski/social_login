<?php

/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') ) {
    exit();
}


class SocialProfile {

    private $settings   = array( );
    private $formErrors = array( );
    private $errors     = array( );
    private $infos      = array( );
    private $user;
    private $userSocial;

    public function __construct(&$page, $params) {
        $this->page   = & $page;
        $this->params = $params;

        /**
         * DEFAULT SETTINGS TO BE OVERRIDEN
         * in "settings" Page Parts of "Social Profile" pages
         * in form of
         * <code>
         *   # hash denotes comments
         *   # string values without quotes
         *   slug_login => another_login
         *   
         *   # boolean values as 0 or 1
         *   show_something => 1
         *   show_other_thing => 0
         * 
         *   # integer values explicitly
         *   avatar_size => 48
         * 
         * </code>
         */
        $this->settings = array(
                    // Slugs
                    'slug_login'             => 'login',
                    'slug_logout'            => 'logout',
                    'slug_register'          => 'register',
                    'slug_forgot'            => 'forgot',
                    // Display settings
                    'template'               => 'default',
                    'part_for_content'       => 'body',
                    'language'               => 'en',
                    'language_locale'        => 'en_US',
                    'datetime_format'        => '%A %e %B %Y, %H:%M',
                    'default_css'            => 1,
                    'icons_uri'              => 'wolf/plugins/social_login/icons/services',
                    // Profile page
                    'show_welcome_header'    => 1,
                    'show_account_info'      => 1,
                    'show_account_manage'    => 1,
                    'show_social_identities' => 1,
                    // Account info settings
                    'show_avatar'            => 1,
                    'avatar_size'            => 48,
                    'show_created_on'        => 1,
                    'show_last_login'        => 1,
                    'show_last_failure'      => 1,
                    'show_username'          => 1,
                    'show_roles'             => 1,
                    'show_permissions'       => 1,
                    'show_actions'           => 1,
                    // Account Manage settings
                    'edit_username'          => 1,
                    'edit_email'             => 1,
                    'edit_password'          => 1,
                    // Social identities settings
        );

        /**
         * getting settings from page part
         * and extending defaults
         */
        if ( $this->page->partExists('settings') )
            $this->settings = array_merge($this->settings, mmCore::parseValues($this->page->part->settings->content_html, '=>', '#'));

        /**
         * Set locale
         */
        $oldI18nClassLocale = I18n::getLocale();
        $oldSetlocale       = setlocale(LC_TIME, '0');
        I18n::setLocale($this->settings['language']);
        setlocale(LC_TIME, $this->settings['language_locale']);

        $dir      = PLUGINS_ROOT . DS . 'social_login' . DS . 'i18n' . DS;
        $i18nFile = $dir . I18n::getLocale() . '-message.php';

        if ( file_exists($i18nFile) ) {
            $array = include $i18nFile;
            I18n::add($array);
        }

        // if ( SOCIAL_DEBUG )
        //    echo 'DEBUG: using language: ' . $language;

        /**
         * Store this url for backward redirections
         * after login/register actions
         */
        Social::setLastUrl();

        // preparing userinfo
        $this->userSocial = false;
        $this->user       = AuthUser::getRecord();

        if ( $this->user )
            $this->userSocial = UserSocial::findByWolfUserId($this->user->id);

        switch ( count($params) ) {
            case 0:

                if ( AuthUser::isLoggedIn() ) {
                    $formValues = Flash::get('social_form_values');
                    if ( empty($formValues) ) {
                        /*
                         * Explicit setting values for safety
                         */
                        $formValues = array(
                                    'name'     => $this->user->name,
                                    'username' => $this->user->username,
                                    'email'    => $this->user->email,
                                    'language' => $this->user->language,
                        );
                    }
                    $view = new View(SOCIAL_VIEW_FOLDER . 'frontend_templates/' . $this->settings['template'] . '/page_profile', array(
                                'user'             => $this->user,
                                'social'           => $this->userSocial,
                                'formValues'       => $formValues,
                                'settings'         => $this->settings,
                                'valid_services'   => SocialLoginSettings::getValidServices(),
                                'profile_page_uri' => $this->page->uri(),
                    ));
                } else {
                    $view = new View(SOCIAL_VIEW_FOLDER . 'frontend_templates/' . $this->settings['template'] . '/page_login', array(
                                'user'             => $this->user,
                                'social'           => $this->userSocial,
                                'formValues'       => Flash::get('social_form_values'),
                                'settings'         => $this->settings,
                                'valid_services'   => SocialLoginSettings::getValidServices(),
                                'profile_page_uri' => $this->page->uri(),
                    ));
                }
                $this->_insertViewToPagePart($view);
                break;
            case 1:
                $subPageSlug = $params[0];
                /**
                 * Logout handling by Members_behaving_page/logout URL
                 */
                // ====================================================== LOGOUT
                if ( ($subPageSlug) === $this->settings['slug_logout'] ) {
                    // Store url
                    Social::setLastUrl($this->page->url());
                    header('HTTP/1.1 302 Found', true);
                    header('Location: ' . URL_PUBLIC . SOCIAL_LOGOUT_URI);
                    die();
                    // ================================================== LOGIN
                } elseif ( ($subPageSlug) === $this->settings['slug_login'] ) {
                    $this->_handleLogin();
                    // ================================================== FORGOT
                } elseif ( ($subPageSlug) === $this->settings['slug_forgot'] ) {
                    $this->page->title = 'Password reset form';
                    if ( get_request_method() == 'POST' ) {
                        $this->_handleForgot();
                    } else {
                        $view = new View(SOCIAL_VIEW_FOLDER . 'frontend_templates/' . $this->settings['template'] . '/page_forgot', array(
                                    'registerData'      => $this->user,
                                    'settings'          => $this->settings,
                                    'formValues'        => Flash::get('social_form_values'),
                                    'frontend_template' => $this->settings['template'],
                                    'profile_page_uri'  => $this->page->uri(),
                        ));
                        $this->_insertViewToPagePart($view);
                    }
                    // ================================================== REGISTER
                } elseif ( ($subPageSlug) === $this->settings['slug_register'] ) {
                    $this->page->title = 'Register new account';
                    if ( (bool) SocialLoginSettings::get('email_enabled') ) {
                        if ( get_request_method() == 'POST' ) {
                            $this->_handleRegister();
                        } else {
                            $view = new View(SOCIAL_VIEW_FOLDER . 'frontend_templates/' . $this->settings['template'] . '/page_register', array(
                                        'registerData'      => $this->user,
                                        'settings'          => $this->settings,
                                        'formValues'        => Flash::get('social_form_values'),
                                        'frontend_template' => $this->settings['template'],
                                        'profile_page_uri'  => $this->page->uri(),
                            ));
                            $this->_insertViewToPagePart($view);
                        }
                    }
                    else
                        $this->_redirectError(__('Email registration is off.'));
                    // ================================================== SAVE 
                } elseif ( ($subPageSlug) === 'save' ) {
                    $this->_handleAccountEdit();
                } else {
                    $this->_displayPage($subPageSlug);
                }
                break;
            default:
                pageNotFound();
                break;
        }
        /**
         * Restore locale
         */
        I18n::setLocale($oldI18nClassLocale);
        setlocale(LC_TIME, $oldSetlocale);

    }


    /**
     * Insert View into page part taken
     * from $this->settings['part_for_content']
     *
     * @param View $view
     */
    private function _insertViewToPagePart($view) {
        // Check if "part_for_content"
        $part_for_content = $this->settings['part_for_content'];

        // Create page part if not exists
        if ( !$this->page->partExists($part_for_content) )
            $this->page->part->$part_for_content               = new PagePart ( );
        $this->page->part->$part_for_content->content_html = $view->render();

    }


    private function _handleLogin() {
        if ( get_request_method() == 'POST' ) {

            /**
             * cleanup data
             */
            $loginData = $_POST['login'];
            array_walk($loginData, create_function('&$val', '$val = trim($val);'));
            use_helper('Validate');

            if ( AuthUser::login($loginData['username'], $loginData['password'], isset($loginData['remember'])) ) {
                // Observer::notify('social_login_login_success', $data['username']);
                //define('CMS_BACKEND', true);
                Observer::notify('admin_login_success',$loginData['username']);
                //dashboard_log_event(__('<b>:name</b> (:username) logged in', array( ':name'     => $loginData['name'], ':username' => $loginData['username'] )), 'social_login', DASHBOARD_LOG_NOTICE);
                //define('CMS_BACKEND', false);
                if ( strlen(Social::getLoginRequiredUrl()) > 0 ) {
                    $url = Social::getLoginRequiredUrl();
                    Social::setLoginRequiredUrl(null, true);
                } else {
                    $url = Social::getLastUrl();
                    Social::setLastUrl(null, true);
                }
                $this->_redirectSuccess(__('Login successful!'), $url);
            } else {
                // Observer::notify('social_login_login_failed', $data['username']);
                Flash::set('social_form_values', $loginData);
                $this->_redirectError(__('Login failed. Please check your login data and try again.'));
            }
        } else {
            header('HTTP/1.1 302 Found', true);
            header('Location: ' . $this->page->url(false));
        }

    }


    private function _validateCaptcha() {

        if ( !isset($_SESSION[SOCIAL_CAPTCHA_KEY]) || !isset($_POST[SOCIAL_CAPTCHA_KEY]) )
            return false;

        if ( trim($_SESSION[SOCIAL_CAPTCHA_KEY]) !== trim($_POST[SOCIAL_CAPTCHA_KEY]) ) {
            return false;
        }
        else
            return true;

    }


    private function _handleRegister() {
        if ( !isset($_POST['register']) ) {
            $this->_redirectError(__('No data sent!'));
        }

        /**
         * cleanup data
         */
        $registerData = $_POST['register'];
        array_walk($registerData, create_function('&$val', '$val = trim($val);'));
        use_helper('Validate');

        // PASSWORD & CONFIRM
        if ( !empty($registerData['password']) && !empty($registerData['confirm']) ) {
            // confirm match
            if ( ($registerData['password'] !== $registerData['confirm'] ) ) {
                $this->_addFormError('password', __('Password and confirmation are not the same!'));
            }
            // length
            if ( strlen($registerData['password']) <= SocialLoginSettings::get('min_password_length') ) {
                $this->_addFormError('password', __('Password must contain at least <b>:num characters</b>!', array(
                            ':num' => SocialLoginSettings::get('min_password_length')
                )));
            }
        } else {
            $this->_addFormError('password', __('Please provide password!'));
            $this->_addFormError('confirm', __('Please provide password confirm!'));
        }

        // NAME
        if ( !empty($registerData['name']) ) {
            // length
            if ( strlen($registerData['name']) < SocialLoginSettings::get('min_username_length') )
                $this->_addFormError('name', __('Name must contain at least <b>:num characters</b>!', array(
                            ':num' => SocialLoginSettings::get('min_username_length')
                )));
        }
        else
            $this->_addFormError('name', __('Please your name!'));


        // USERNAME
        if ( !empty($registerData['username']) ) {
            // format
            if ( !Validate::alpha_dash($registerData['username']) )
                $this->_addFormError('username', __('Username must consist of latin letters, digits and "-" or "_" characters!'));

            // length
            if ( strlen($registerData['username']) < SocialLoginSettings::get('min_username_length') )
                $this->_addFormError('username', __('Username must contain at least <b>:num characters</b>!', array(
                            ':num' => SocialLoginSettings::get('min_username_length')
                )));

            // unique
            if ( $existingUser = User::findBy('username', $registerData['username']) ) {
                if ( $existingUser->username !== AuthUser::getUserName() ) {
                    $this->_addFormError('username', __('This username is already taken - :username', array(
                                ':username' => $registerData['username'],
                    )));
                }
            }

            // check if password is !== username
            if ( !empty($registerData['password']) &&
                        ( strtolower($registerData['username']) == strtolower($registerData['password']) ) )
                $this->_addFormError('password', __('Password cannot be the same as the username!'));
        }
        else
            $this->_addFormError('username', __('Please your username!'));

        // EMAIL
        if ( !empty($registerData['email']) ) {
            // format
            if ( !Validate::email($registerData['email']) )
                $this->_addFormError('email', __('Invalid email address!'));
            // unique
            if ( Record::countFrom('User', 'email=:email', array( ':email' => $registerData['email'] )) > 0 )
                $this->_addFormError('email', __('Cannot use email: <b>:email</b> Other user already uses this email!', array( ':email' => $registerData['email'] )));
            /*
              // pending registration
              if ( Record::countFrom('UserPending', 'email=:email', array( ':email' => $registerData['email'] )) > 0 )
              $this->_addFormError('email', __('This email is already pending registration! - :email', array( ':email' => $registerData['email'] )));
             * 
             */
        }
        else
            $this->_addFormError('email', __('Please provide your email address!'));

        // CAPTCHA
        if ( !$this->_validateCaptcha() ) {
            Flash::set('social_form_values', $registerData);
            $this->_addError(__('Invalid security check. Try again!'));
            $this->_addFormError('captcha', __('Invalid security check. Try again!'));
        }


        if ( !empty($this->formErrors) ) {
            // set values to fill up errorneous form
            Flash::set('social_form_values', $registerData);
            $this->_redirectError(__('There are some errors in form'), $this->page->url(false) . '/' . $this->settings['slug_register']);
        }

        unset($registerData['confirm']);

        /**
         *  Check if this email is pending confirmation
         *  if yes - replace the record
         *  if no  - create new record
         */
        if ( $userPending = Record::findOneFrom('UserPending', 'email=:email', array( ':email' => $registerData['email'] )) ) {
            $this->_addInfo(__('This email already submitted registration.'));
        }
        else
            $userPending = new UserPending();


        // PREPARE
        $reg_salt = AuthUser::generateSalt();
        $userPending->setFromData(array(
                    'name'     => $registerData['name'],
                    'username' => $registerData['username'],
                    'email'    => $registerData['email'],
                    'password' => AuthUser::generateHashedPassword($registerData['password'], $reg_salt),
                    'reg_hash' => sha1(microtime()),
                    'reg_date' => date('Y-m-d H:i:s'),
                    'reg_salt' => $reg_salt,
        ));

        // SAVE
        if ( $userPending->save() ) {
            use_helper('Email');
            $email = new Email();
            $email->from(Setting::get('admin_email'), Setting::get('admin_title'));
            $email->to($userPending->email);
            $email->subject(__('New account registration in :site_title', array( ':site_title' => Setting::get('admin_title') )));
            $email->message(
                        __('Hello, :name', array( ':name' => $userPending->name )) . "\n" . "\n" .
                        __('You have registered new account in :site_title.', array( ':site_title' => Setting::get('admin_title') )) . ' ' .
                        __('To activate your account visit the following address:') . "\n" . "\n" .
                        URL_PUBLIC . SOCIAL_CONFIRM_EMAIL_URI . '/' . $userPending->reg_hash . "\n" . "\n" .
                        __('If you did not submit account registration form, please ignore this email.')
            );
            if ( $email->send() ) {
                $this->_redirectSuccess(__('Check your mailbox for confirmation email with link to activate your account!'), $this->page->url(false));
            } else {
                Flash::set('social_form_values', $registerData);
                $this->_redirectError(__('Unable to send email with new password! Try again later or contact administrator.'), $this->page->url(false) . '/' . $this->settings['slug_register']);
            }
        } else {
            Flash::set('social_form_values', $registerData);
            $this->_redirectError(__('Error creating new account. Please contact administrator!'), $this->page->url(false) . '/' . $this->settings['slug_register']);
        }

    }


    private function _handleForgot() {
        if ( !isset($_POST['forgot']) ) {
            $this->_redirectError(__('No data sent!'));
        }

        /**
         * cleanup data
         */
        $forgotData = $_POST['forgot'];
        array_walk($forgotData, create_function('&$val', '$val = trim($val);'));
        use_helper('Validate');

        // check captcha and if failed return filtered data
        if ( !$this->_validateCaptcha() ) {
            Flash::set('social_form_values', $forgotData);
            $this->_redirectError(__('Invalid security check. Try again!'), $this->page->url(false) . '/' . $this->settings['slug_forgot']);
        }

        if ( !empty($forgotData['email']) ) {
            // validate email
            if ( !Validate::email($forgotData['email']) )
                $this->_addFormError('email', __('Invalid email address!'));
        }
        else
            $this->_addFormError('email', __('Please provide your email address!'));

        if ( $user = User::findBy('email', $forgotData['email']) ) {
            use_helper('Email');
            $new_pass   = 'mm' . dechex(rand(100000, 4294967)) . 'K';
            // generate salt if empty
            if ( empty($user->salt) )
                $user->salt = AuthUser::generateSalt();

            $user->password = AuthUser::generateHashedPassword($new_pass, $user->salt);
            if ( !$user->save() ) {
                $this->_redirectError(__('Error generating new password!'), $this->page->url(false) . '/' . $this->settings['slug_forgot']);
            }

            $email = new Email();
            $email->from(Setting::get('admin_email'), Setting::get('admin_title'));
            $email->to($user->email);
            $email->subject(__('Your new password from ') . Setting::get('admin_title'));
            $email->message(
                        __('Username') . ': ' . $user->username . "\n" .
                        __('Password') . ': ' . $new_pass
            );
            if ( $email->send() ) {
                if ( SOCIAL_DEBUG )
                    echo '<pre>' . print_r($email, true) . '</pre>';
                $this->_redirectSuccess(__('An email has been sent with your new password!'), $this->page->url(false));
            } else {
                $this->_redirectError(__('Unable to send email with new password! Try again later or contact administrator.'), $this->page->url(false) . '/' . $this->settings['slug_forgot']);
            }
        }
        else
            $this->_addFormError('email', __('This email is not registered in our site!'));

        if ( !empty($this->formErrors) ) {
            // set values to fill up errorneous form
            Flash::set('social_form_values', $forgotData);
            $this->_redirectError(__('There are errors in the submitted form'), $this->page->url(false) . '/' . $this->settings['slug_forgot']);
        }

    }


    private function _handleAccountEdit() {
        if ( !isset($_POST['user']) )
            $this->_redirectError(__('No data sent!'));

        /**
         * cleanup data
         */
        $userData = $_POST['user'];
        array_walk($userData, create_function('&$val', '$val = trim($val);'));
        use_helper('Validate');

        if ( $this->user ) {
            use_helper('Validate');

            // check if user wants to change the password
            if ( strlen($userData['password']) > 0 ) {
                // check if pass and confirm are egal and >= 5 chars
                if ( strlen($userData['password']) >= SocialLoginSettings::get('min_password_length') && $userData['password'] === $userData['confirm'] ) {
                    unset($userData['confirm']);
                }
                else
                    $this->_addFormError('password', __('Password and Confirm were <b>not the same</b> or <b>too short</b>!'));
            } else {
                unset($userData['password'], $userData['confirm']);
            }

            // check if password is !== username
            if ( !empty($userData['username']) && !empty($userData['password']) &&
                        ( strtolower($userData['username']) == strtolower($userData['password']) ) )
                $this->_addFormError('password', __('Password cannot be the same as the <b>username</b>!'));

            // check if username >= 3 chars
            if ( isset($userData['username']) && (strlen($userData['username']) < SocialLoginSettings::get('min_username_length') ) )
                $this->_addFormError('username', __('Username must contain at least <b>:num characters</b>!', array(
                            ':num' => SocialLoginSettings::get('min_username_length')
                )));

            // check if name >= 3 chars
            if ( isset($userData['name']) && (strlen($userData['name']) < SocialLoginSettings::get('min_username_length') ) )
                $this->_addFormError('name', __('Name must contain at least <b>:num characters</b>!', array(
                            ':num' => SocialLoginSettings::get('min_username_length')
                )));


            if ( isset($userData['username']) ) {
                // validate username validity
                if ( !Validate::alpha_dash($userData['username']) )
                    $this->_addFormError('username', __('Username must consist of latin letters, digits and "-" or "_" characters!'));
                // check if username already exists
                if ( $existingUser = User::findBy('username', $userData['username']) ) {
                    if ( $existingUser->username !== AuthUser::getUserName() ) {
                        $this->_addFormError('username', __('This username is already taken - :username', array(
                                    ':username' => $userData['username'],
                        )));
                    }
                }
            }


            if ( !empty($userData['email']) ) {
                // validate email
                if ( !Validate::email($userData['email']) )
                    $this->_addFormError('email', __('Invalid email address!'));
                // check if other user has this email
                if ( ( Record::countFrom('User', 'email=:email', array( ':email' => $userData['email'] )) > 0 ) && (AuthUser::getRecord()->email !== $userData['email']) )
                    $this->_addFormError('email', __('Cannot use email: <b>:email</b> Other user already uses this email!', array( ':email' => $userData['email'] )));
            }

            // if password is set and validated, generate hashed pass
            if ( isset($userData['password']) ) {
                if ( empty($this->user->salt) ) {
                    $this->user->salt = AuthUser::generateSalt();
                }
                $userData['password'] = AuthUser::generateHashedPassword($userData['password'], $this->user->salt);
            }



            if ( !empty($this->formErrors) ) {
                // set values to fill up errorneous form
                Flash::set('social_form_values', $userData);
                $this->_redirectError(__('There are some errors in form'));
            }

            // username HAS CHANGED!
            if ( isset($userData['username']) ) {
                if ( $this->user->username !== $userData['username'] ) {
                    // set new $_SESSION username info
                    $_SESSION[AuthUser::SESSION_KEY]['username'] = $userData['username'];
                }
            }
            $this->user->setFromData($userData);

            if ( $this->user->save() ) {
                // todo: Observer::notify()
                $this->_redirectSuccess(__('Saved changes!'));
                //Observer::notify('user_after_edit', $user->name);
            } else {
                // $data = '<pre>' . print_r($this->user, true) . '</pre>';
                Flash::set('social_form_values', $userData);
                $this->_redirectError(__('Error saving changes!'));
            }
        }
        else
            die('you are not logged in!');

    }


    private function _addFormError($field, $msg) {
        $this->formErrors[$field][] = $msg;

    }


    private function _addError($msg) {
        $this->errors[] = $msg;

    }


    private function _addInfo($msg) {
        $this->infos[] = $msg;

    }


    private function _redirectError($msg, $url = false) {
        array_push($this->errors, $msg);
        Flash::set('error', $msg);
        Flash::set(SOCIAL_FLASH_ERROR, $this->errors);
        Flash::set(SOCIAL_FLASH_INFO, $this->infos);
        Flash::set(SOCIAL_FLASH_FORM_ERROR, $this->formErrors);

//        echo '<pre>' . print_r($this->formErrors, true) . '</pre>';
        // redirect to behavior page by default
        if ( !$url ) {
            $url = $this->page->url(false);
        }

        if ( !SOCIAL_DEBUG ) {
            header('HTTP/1.1 302 Found', true);
            header('Location: ' . $url);
            die();
        } else {
            echo '<br>DEBUG: _redirectError(), Continue to <a href="' . $url . '"> ' . $url . '</a>';
            die();
        }

    }


    private function _redirectSuccess($msg, $url = false) {
        // array_push($this->successes, $msg);
        Flash::set('success', $msg);
        Flash::set(SOCIAL_FLASH_SUCCESS, $msg);
        Flash::set(SOCIAL_FLASH_INFO, $this->infos);

        // redirect to behavior page by default
        if ( !$url ) {
            $url = $this->page->url(false);
        }

        if ( !SOCIAL_DEBUG ) {
            header('HTTP/1.1 302 Found', true);
            header('Location: ' . $url);
            die();
        } else {
            echo '<br>DEBUG: _redirectSuccess(),  Continue to <a href="' . $url . '"> ' . $url . '</a>';
            die();
        }

    }


    private function _displayPage($slug) {
        if ( !$this->page = Page::findBySlug($slug, $this->page) )
            page_not_found();

    }


}


class PageSocialProfile extends Page {

    /*
      protected function setUrl() {
      $use_date = Plugin::getSetting('use_dates', 'archive');
      if ($use_date === '1') {
      $this->url = trim($this->parent->url . date('/Y/m/d/', strtotime($this->created_on)) . $this->slug, '/');
      }
      elseif ($use_date === '0') {
      $this->url = trim($this->parent->url . '/' . $this->slug, '/');
      }
      }
     *
     */

    public function title() {
        return 'Social';
//return isset($this->time) ? strftime($this->title, $this->time) : $this->title;

    }


    public function breadcrumb() {
        return 'Social';
//return isset($this->time) ? strftime($this->breadcrumb, $this->time) : $this->breadcrumb;

    }


}


