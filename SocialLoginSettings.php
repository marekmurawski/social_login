<?php

/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') ) {
    exit();
}


class SocialLoginSettings {

    private static $settings      = array( );
    public static $validServices = false;
    public static $services      = array(
                'google'   => 'google',
                'facebook' => 'facebook',
                'twitter'  => 'twitter',
                'github'   => 'github',
    );

    /**
     *
     * @param type $id
     * @return boolean
     */
    public static function getValidServices() {
        if ( self::$validServices )
            return self::$validServices;

        $arr = self::$services;

        $googleInvalid = (strlen(self::get('google_client_id')) < 1) || (strlen(self::get('google_client_secret')) < 1);
        $googleEnabled = (bool) self::get('google_enabled');
        if ( $googleInvalid || !$googleEnabled ) {
            unset($arr['google']);
        }

        $facebookInvalid = (strlen(self::get('facebook_client_id')) < 1) || (strlen(self::get('facebook_client_secret')) < 1);
        $facebookEnabled = (bool) self::get('facebook_enabled');
        if ( $facebookInvalid || !$facebookEnabled ) {
            unset($arr['facebook']);
        }

        $twitterInvalid = (strlen(self::get('twitter_client_id')) < 1) || (strlen(self::get('twitter_client_secret')) < 1);
        $twitterEnabled = (bool) self::get('twitter_enabled');
        if ( $twitterInvalid || !$twitterEnabled ) {
            unset($arr['twitter']);
        }

        if ( count($arr) > 0 ) {
            self::$validServices = $arr;
            return $arr;
        }
        else
            return false;

    }


    /**
     *
     * @param type $id
     * @return boolean
     */
    public static function get($id) {
        if ( count(self::$settings) < 1 ) {
            self::$settings = Plugin::getAllSettings('social_login');
        }
        if ( isset(self::$settings[$id]) ) {
            return self::$settings[$id];
        } else {
            return false;
        }

    }


}


?>
