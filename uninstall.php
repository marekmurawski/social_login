<?php

/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') ) {
    exit();
}
$conn = Record::getConnection();

$conn->exec("SET FOREIGN_KEY_CHECKS = 0;");
$conn->exec("DROP TABLE IF EXISTS " . TABLE_PREFIX . "user_social");
$conn->exec("DROP TABLE IF EXISTS " . TABLE_PREFIX . "user_pending");
$conn->exec("SET FOREIGN_KEY_CHECKS = 1;");


Plugin::deleteAllSettings('social_login');

Flash::set('success', 'Social Login Plugin: Dropped table and removed settings!');

exit();