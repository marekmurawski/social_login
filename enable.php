<?php

/*
 * Wolf CMS - Content Management Simplified. <http://www.wolfcms.org>
 * Copyright (C) 2008-2010 Martijn van der Kleijn <martijn.niji@gmail.com>
 *
 * Members Plugin for Wolf CMS
 * Provides OAuth social login and account management.
 *
 * @package Plugins
 * @subpackage social_login
 *
 * @author Marek Murawski <http://marekmurawski.pl>
 * @copyright Marek Murawski, 2013
 * @license http://www.gnu.org/licenses/gpl.html GPLv3 license
 *
 */
/* Security measure */
if ( !defined('IN_CMS') ) {
    exit();
}
if ( !Plugin::isEnabled('mm_core') ) {
    unset(Plugin::$plugins['social_login']);
    Plugin::save();
//    Plugin::deactivate('social_login');
    Flash::set('error', '<b>mm_core</b> plugin not found! <br/>
                            Download latest <b>mm_core</b> here: <br/>
                            <a href="https://bitbucket.org/marekmurawski/mm_core" target="_blank">https://bitbucket.org/marekmurawski/mm_core</a>
                           ');
    exit();
}

// load mmInstaller from mm_core plugin
AutoLoader::addFile('mmInstaller', PLUGINS_ROOT . '/mm_core/lib/mmInstaller.php');

$installer = new mmInstaller('social_login');

// Create database tables
$PDO    = Record::getConnection();
$driver = strtolower($PDO->getAttribute(Record::ATTR_DRIVER_NAME));
if ( $driver != 'mysql' ) {
    unset(Plugin::$plugins['social_login']);
    Plugin::save();
//    Plugin::deactivate('social_login');
//    Flash::set('error', 'This plugin requires <b>MySQL</b> to work.');
    exit();
}

$table_user_social = "
CREATE TABLE IF NOT EXISTS `" . TABLE_PREFIX . "user_social` (
  `id`              INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id`         INT(11) unsigned DEFAULT NULL,
  `service_type`    VARCHAR(100) NOT NULL DEFAULT '',
  `refresh_token`   VARCHAR(200) NOT NULL DEFAULT '',
  `social_id`       VARCHAR(200) NOT NULL DEFAULT '',
  `social_name`     VARCHAR(200) NOT NULL DEFAULT '',
  `social_email`    VARCHAR(100) DEFAULT NULL,
  `json`            TEXT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service_key` (`social_id`,`service_type`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
";

$table_user_pending = "
CREATE TABLE IF NOT EXISTS `" . TABLE_PREFIX . "user_pending` (
    `id`       INT(11)       unsigned NOT NULL AUTO_INCREMENT,
    `name`     VARCHAR(100)  default NULL,
    `email`    VARCHAR(255)  default NULL,
    `username` VARCHAR(40)          NOT NULL,
    `password` VARCHAR(1024) default NULL,
    `reg_salt` VARCHAR(1024) default NULL,
    `reg_hash` VARCHAR(1024) default NULL,
    `reg_date` DATETIME      default NULL,
    PRIMARY KEY  (`id`),
    UNIQUE KEY `username` (`username`),
    UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
";

if ( $PDO->exec($table_user_social) === false ) {
    $installer->logError(__('Unable to create DB table') . ' user_social');
    print_r($PDO->errorInfo());
}

if ( $PDO->exec($table_user_pending) === false ) {
    $installer->logError(__('Unable to create DB table') . ' user_pending');
    print_r($PDO->errorInfo());
}

/*
  if ( $driver == 'sqlite' ) {

  $table_user_social = "
  CREATE TABLE IF NOT EXISTS " . TABLE_PREFIX . "user_social (
  id int(11) PRIMARY KEY,
  user_id int(11) DEFAULT NULL,
  service_type varchar(100) NOT NULL DEFAULT '',
  refresh_token varchar(200) NOT NULL DEFAULT '',
  social_id varchar(200) NOT NULL DEFAULT '',
  social_name varchar(200) NOT NULL DEFAULT '',
  social_email varchar(100) DEFAULT NULL,
  json text
  );
  ";

  $table_user_pending = "
  CREATE TABLE IF NOT EXISTS " . TABLE_PREFIX . "user_pending (
  id int(11) PRIMARY KEY,
  name varchar(100) default NULL,
  email varchar(255) default NULL,
  username varchar(40) NOT NULL,
  password varchar(40) default NULL,
  reg_hash varchar(40) default NULL,
  reg_date varchar(40) default NULL
  );
  ";

  if ( $PDO->exec($table_user_social) === false ) {
  $installer->logError(__('Unable to create DB table') . ' user_social');
  print_r ($PDO->errorInfo());
  }
  if ( $PDO->exec($table_user_pending) === false ) {
  $installer->logError(__('Unable to create DB table') . ' user_pending');
  print_r ($PDO->errorInfo());
  }
  }
 * 
 */

$installer->createPermission('social_login');
$installer->createPermission('social_edit_account');
$installer->createPermission('social_edit_username');

/**
 * For catching translation string which is used in Wolf's /user/edit/#id
 * __('Social user')
 * __('Social User')
 */
$installer->createRole('social user');

$installer->assignPermissionToRole('social_login', 'social user');
$installer->assignPermissionToRole('social_edit_account', 'social user');
$installer->assignPermissionToRole('social_edit_username', 'social user');
$settings = array(
            'debug_mode'             => 0,
            'newuser_roles'          => 'social user',
            'min_password_length'    => 5,
            'min_username_length'    => 3,
            // EMAIL
            'email_enabled'          => 1,
            // GOOGLE
            'google_enabled'         => 1,
            'google_client_id'       => '',
            'google_client_secret'   => '',
            'google_offline'         => 0,
            'google_scope'           => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile',
            // GITHUB
            'github_enabled'         => 1,
            'github_client_id'       => '',
            'github_client_secret'   => '',
            'github_offline'         => 0,
            'github_scope'           => 'user:email',
            // FACEBOOK
            'facebook_enabled'       => 1,
            'facebook_client_id'     => '',
            'facebook_client_secret' => '',
            'facebook_offline'       => 0,
            'facebook_scope'         => 'email',
            // TWITTER
            'twitter_enabled'        => 1,
            'twitter_client_id'      => '',
            'twitter_client_secret'  => '',
            'twitter_offline'        => 0,
);
Plugin::setAllSettings($settings, 'social_login');

exit();