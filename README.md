Members Plugin for Wolf CMS
===========================

## NOTE: THIS IS WORK IN PROGRESS (don't use for production sites yet)


Provides OAuth social login and account management. 
**(Google, Facebook, Twitter and Github)**

This plugin implements External Authority Login functionality using OAuth/OAuth2
protocol. The standard flow of sign-up process looks like this (google as example):

1. Visitor clicks "Connect with Google"
2. He is redirected to Google servers.
3. (Optional) If user is not logged in to Google - he logs in there
4. Google asks visitor if he wants to grant us access to **some** of his data
5. If he agrees, he is taken back to Wolf site
    - if his **service-provided email** is **NOT FOUND** in `User` table  
      new Account is created for visitor and he becomes User with
      `social user` role assigned and email set according to social data (or empty)
    - if his **service-provided email** is **Found** in `User` table  
      the visitor is logged in as the User having this email and his social  
      identity is connected to his Account

## Features:

 - visitors self-registration using google / facebook / twitter / github providers
 - classic email registration process (with captcha + activation email)
 - frontend account management
 - attach / detach social identities from account
 - ability to change username
 - css/templates customizable profile page
 - frontend login redirection for every request needing login
 - full i18n support (thanks Transifex)

## Todo's:

 - intelligent page choice when login is needed _(based on language -> url/cookie/I18n/user preference)_
 - registered user **language** _detection/fetching/editing_
 - option to **delete account** in frontend
 - option to require **email confirmation** upon **email address change**
 - option to prevent social identity transferring between Accounts
 - customizable email templates
 - Account plugin integration
 - Dashboard plugin integration


Requirements
------------

- Wolf CMS **0.7.7** __(should work with 0.7.5 but I didn't test)__
- [mm_core plugin](https://bitbucket.org/marekmurawski/mm_core)
- mod_rewrite ON
- mySQL _(SQLite will be supported too)_
- GD library _(for captcha)_
- your site must be **visible in DNS**
- Google, Facebook, Twitter, Github account


Installation
------------

Social Login plugin can be installed into your WolfCMS by uploading it to
**CMS_ROOT/wolf/plugins/social_login/** and enabling it in administration panel.

### 1. Services setup

After enabling Social Login you'll need to setup your Social Applications _(or 
whatever they are called)_ in:

- Google -  https://code.google.com/apis/console/b/0/
- Facebook - https://developers.facebook.com/apps
- Twitter - https://dev.twitter.com/apps
- Github - https://github.com/settings/applications

For brief description of Apps setup process go to Social Login Settings in Backend.

### 2. Creating profile page(s)

In order to create `Social Profile` pages, where users will be able to login
and manage their accounts you need to do is **set** `Social Profile` **behavior** 
_(Page Type)_ for a Wolf CMS Page of your choice.

### 3. Customizing

You can customize profile page by creating `settings` **page part** with the
following content *(copy&paste it, then tweak&play&experiment)*:

```text

# DEFAULT SETTINGS TO BE OVERRIDDEN
#######################################
# in "settings" Page Part 
# of "Social Profile" pages
#
# hash as FIRST character means comment
#
# in the following format
#
# STRING values without quotes:
# slug_login => another_login
#   
# BOOLEAN values as 0 or 1
# show_something    => 1
# show_other_thing  => 0
# 
# INTEGER values explicitly
# avatar_size => 48

# GENERAL SETTINGS
##################
template               => default
part_for_content       => body
language               => en
language_locale        => en_US

# Slugs
#######
slug_login             => login
slug_logout            => logout
slug_register          => register
slug_forgot            => forgot

# Display settings
##################
default_css            => 1
icons_uri              => wolf/plugins/social_login/icons/services
show_alerts            => 1

# PROFILE PAGE
##############
show_welcome_header    => 1
show_account_info      => 1
show_account_manage    => 1
show_social_identities => 1

# PROFILE PAGE Account info 
###########################
datetime_format        => %A %e %B %Y %H:%M
show_avatar            => 1
avatar_size            => 48
show_created_on        => 1
show_last_login        => 1
show_last_failure      => 1
show_username          => 1
show_roles             => 1
show_permissions       => 1
show_actions           => 1

# PROFILE PAGE Account Manage
#############################
edit_username          => 1
edit_email             => 1
edit_password          => 1

# PROFILE PAGE Social identities
################################
# nothing to cofigure yet :)

```

There's also small login box function included with this plugin.

You can include it in your Layout, Page or Snippet _(preferably - Layout)_

Social::loginBox provides quick social signup and returns user to the last url 
which required login. _(try setting some page's to login "required" to see the effect)_

```php
<?php
if (Plugin::isEnabled('social_login')) {
      Social::loginBox(array(
              'template'       => 'default', // only this available now
              'language'       => 'en', 
              'default_css'    => 1, // set to 0 if you want to use your own css
              'show_username'  => 1,
              'show_logout'    => 1,
              'show_actions'   => 1,
              'show_connected' => 1,
              'show_avatar'    => 1,
              'show_labels'    => 1,
              'avatar_size'    => 32,
              'show_alerts'    => 1,
      ));
}
?>
```

If you'd like to translate this plugin into your language, you can edit `/i18n/xx-message.php' 
or **better** do it easily and conveniently using Transifex at 
[https://www.transifex.com/accounts/profile/mm/](https://www.transifex.com/accounts/profile/mm/)

I'll provide more customization options in **first stable release**


Changelog
---------

##### 0.1.0

- first release (work in progress)


License
-------

* Plugin - GPLv3 license
* OAuth API library - Manuel Lemos
* Captcha - GPLv3 - Jose Rodriguez
* Default social icons - GPLv3

Disclaimer
----------

While I make every effort to deliver quality plugins for Wolf CMS, I do not guarantee that they are free from defects. They are provided “as is," and you use it at your own risk. I'll be happy if you notice me of any errors.

I'm not really programmer nor web developer, however I like programming PHP and JavaScript. In fact I'm an [architekt](http://marekmurawski.pl).
